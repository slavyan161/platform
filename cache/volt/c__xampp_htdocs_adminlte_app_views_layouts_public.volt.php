<div class="login-box">
  <div class="login-logo">
    <a href="<?= $this->url->get('#') ?>"><b>Admin</b>LTE</a>
  </div>
  <!-- /.login-logo -->
  <?= $this->getContent() ?>
  <!-- /.login-box-body -->
</div>
