<section class="content-header">
    <h1>
        <?= ucwords($this->router->getControllerName()) ?>
    </h1>
</section>

<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="box box-default color-palette-box">
        <div class="box-body">
            <?= $this->getContent() ?>

            <?php $v9331202781iterated = false; ?><?php $v9331202781iterator = $dataPartner; $v9331202781incr = 0; $v9331202781loop = new stdClass(); $v9331202781loop->self = &$v9331202781loop; $v9331202781loop->length = count($v9331202781iterator); $v9331202781loop->index = 1; $v9331202781loop->index0 = 1; $v9331202781loop->revindex = $v9331202781loop->length; $v9331202781loop->revindex0 = $v9331202781loop->length - 1; ?><?php foreach ($v9331202781iterator as $partner) { ?><?php $v9331202781loop->first = ($v9331202781incr == 0); $v9331202781loop->index = $v9331202781incr + 1; $v9331202781loop->index0 = $v9331202781incr; $v9331202781loop->revindex = $v9331202781loop->length - $v9331202781incr; $v9331202781loop->revindex0 = $v9331202781loop->length - ($v9331202781incr + 1); $v9331202781loop->last = ($v9331202781incr == ($v9331202781loop->length - 1)); ?><?php $v9331202781iterated = true; ?>
            <?php if ($v9331202781loop->first) { ?>
            <table class="table table-bordered table-striped" align="center">
                <thead>
                    <tr>
                        <th width="5%">Partner Id</th>
                        <th>Partner Name</th>
                        <th colspan="2">Partner Type</th>
                       
                    </tr>
                </thead>
                <tbody>
                    <?php } ?>
                    <tr>
                        <td><?= $partner->partnerId ?></td>
                        <td><?= $partner->partnerName ?></td>
                        <td><?= $partner->partnerTypeName ?></td>
                    </tr>
                    <?php if ($v9331202781loop->last) { ?>
                </tbody>
            </table>
            <?php } ?>
            <?php $v9331202781incr++; } if (!$v9331202781iterated) { ?>
            You dont have Permissions to see any Data
            <?php } ?>
        </div>
    </div>
</section>

