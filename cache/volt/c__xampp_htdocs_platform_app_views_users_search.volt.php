<section class="content-header">
    <h1>
        <?= ucwords($this->router->getControllerName()) ?>
    </h1>
</section>

<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="box box-default color-palette-box box-body table-responsive no-padding">
        <div class="box-body">
            <?= $this->getContent() ?>

            <ul class="pager">
                <li class="previous pull-left">
                    <?= $this->tag->linkTo(['users/index', '&larr; Go Back']) ?>
                </li>
                <li class="pull-right">
                    <?= $this->tag->linkTo(['users/create', 'Create users', 'class' => 'btn']) ?>
                </li>
            </ul>

            <?php $v17672699641iterated = false; ?><?php $v17672699641iterator = $page->items; $v17672699641incr = 0; $v17672699641loop = new stdClass(); $v17672699641loop->self = &$v17672699641loop; $v17672699641loop->length = count($v17672699641iterator); $v17672699641loop->index = 1; $v17672699641loop->index0 = 1; $v17672699641loop->revindex = $v17672699641loop->length; $v17672699641loop->revindex0 = $v17672699641loop->length - 1; ?><?php foreach ($v17672699641iterator as $user) { ?><?php $v17672699641loop->first = ($v17672699641incr == 0); $v17672699641loop->index = $v17672699641incr + 1; $v17672699641loop->index0 = $v17672699641incr; $v17672699641loop->revindex = $v17672699641loop->length - $v17672699641incr; $v17672699641loop->revindex0 = $v17672699641loop->length - ($v17672699641incr + 1); $v17672699641loop->last = ($v17672699641incr == ($v17672699641loop->length - 1)); ?><?php $v17672699641iterated = true; ?>
            <?php if ($v17672699641loop->first) { ?>
            <table class="table table-bordered table-striped" align="center">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Profile</th>
                        <th>Banned?</th>
                        <th>Suspended?</th>
                        <th>Confirmed?</th>
                    </tr>
                </thead>
                <tbody>
                    <?php } ?>
                    <tr>
                        <td><?= $user->id ?></td>
                        <td><?= $user->name ?></td>
                        <td><?= $user->email ?></td>
                        <td><?= $user->profile->name ?></td>
                        <td><?= ($user->banned == 'Y' ? 'Yes' : 'No') ?></td>
                        <td><?= ($user->suspended == 'Y' ? 'Yes' : 'No') ?></td>
                        <td><?= ($user->active == 'Y' ? 'Yes' : 'No') ?></td>
                        <td width="12%"><?= $this->tag->linkTo(['users/edit/' . $user->id, '<i class="fa fa-pencil"></i> Edit', 'class' => 'btn btn-default']) ?></td>
                        <td width="12%"><?= $this->tag->linkTo(['users/delete/' . $user->id, '<i class="fa fa-remove"></i> Delete', 'class' => 'btn btn-default', 'onClick' => 'return confirm("Are you sure?");']) ?></td>
                    </tr>
                    <?php if ($v17672699641loop->last) { ?>
                </tbody>
                <tr>
                    <td><?= $page->current ?> / <?= $page->total_pages ?></td>
                    <td colspan="10" align="right">
                        <div class="btn-group">
                            <?= $this->tag->linkTo(['users/search', '<i class="fa fa-fast-backward"></i> First', 'class' => 'btn btn-default']) ?>
                            <?= $this->tag->linkTo(['users/search?page=' . $page->before, '<i class="fa fa-step-backward"></i> Previous', 'class' => 'btn btn-default']) ?>
                            <?= $this->tag->linkTo(['users/search?page=' . $page->next, '<i class="fa fa-step-forward"></i> Next', 'class' => 'btn btn-default']) ?>
                            <?= $this->tag->linkTo(['users/search?page=' . $page->last, '<i class="fa fa-fast-forward"></i> Last', 'class' => 'btn btn-default']) ?>
                            <!-- <span class="help-inline"><?= $page->current ?>/<?= $page->total_pages ?></span> -->
                        </div>
                    </td>
                </tr>
            </table>
            <?php } ?>
            <?php $v17672699641incr++; } if (!$v17672699641iterated) { ?>
            No users are recorded
            <?php } ?>
        </div>
    </div>
</section>