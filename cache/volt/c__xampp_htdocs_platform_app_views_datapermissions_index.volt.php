<section class="content-header">
	<h1>
		<?= ucwords($this->router->getControllerName()) ?>
	</h1>
</section>

<section class="content">
	<!-- Small boxes (Stat box) -->
	<div class="box box-default color-palette-box">
		<div class="box-body">
			<?= $this->getContent() ?>
			<form method="post">
				<div class="col-md-12">
					<div class="row">
						<div class="form-group">
							<label class="col-md-1 control-label">Profile</label>
							<div class="col-md-3">
								<?= $this->tag->select(['profileId', $profiles, 'class' => 'form-control', 'using' => ['id', 'name'], 'useEmpty' => true, 'emptyText' => '...', 'emptyValue' => '']) ?>
							</div>
							<?php if ($this->request->isPost() && $profile) { ?>
								<div class="col-md-3">
								<?= $this->tag->select(['partnersId', $partners, 'class' => 'form-control', 'using' => ['id', 'name'], 'useEmpty' => true, 'emptyText' => '...', 'emptyValue' => '']) ?>
								</div>
							<?php } ?>
							<div class="col-md-3">
								<?= $this->tag->submitButton(['Search', 'class' => 'btn btn-primary', 'name' => 'search']) ?>
							</div>
						</div>
					</div>

					<?php if ($this->request->isPost() && $profile) { ?>
					<?php foreach ($user as $users) { ?>
					<label for='name'><h3><?= ucwords($users->name) ?></h3></label> 
					<table class="table table-bordered table-striped" align="center">
						<thead>
							<tr>
								<th width="1%">^</th>
								<th width="15%">Partner Name</th>
							</tr>
						</thead>
						<tbody>
						<?php $i = 0; ?>
							<?php foreach ($result as $results) { ?>
							
							<tr>
								
								<td> <input type="checkbox" name="permissions[]" value="<?= $users->id . '.' . $results->id . '.' . $results->typeId ?>"  
									<?php if (isset($permissions['partnerId' . '.' . $users->id . '.' . $results->id])) { ?> 
										<?php if ($results->id !== 0 || $results->id !== null) { ?>
											 checked="checked"   
										<?php } ?> 
									<?php } ?> >
									<input type="hidden" name="typePartnersId" value="<?= $results->typeId ?>">
								</td>	
								
								<td> <?= $results->name ?> </td> 

							</tr>
							<?php $i = $i + 1; ?>
							<?php } ?>
						</tbody>
					</table>
					<?php } ?>
					<?= $this->tag->submitButton(['Submit', 'class' => 'btn btn-primary', 'name' => 'submit']) ?>   

					<?php } ?>
				</div>
			</form>
		</div>
	</div>
</section>
