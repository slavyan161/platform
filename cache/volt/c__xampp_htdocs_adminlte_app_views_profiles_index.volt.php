<section class="content-header">
    <h1>
        <?= ucwords($this->router->getControllerName()) ?>
    </h1>
</section>
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="box box-default color-palette-box">
        <div class="box-body">
            <?= $this->getContent() ?>

            <div align="right">
                <?= $this->tag->linkTo(['profiles/create', '<i class=\'icon-plus-sign\'></i> Create Profiles', 'class' => 'btn btn-primary']) ?>
            </div>
            <div class="col-md-6">
                <form role="form" method="post" action="<?= $this->url->get('profiles/search') ?>" autocomplete="off">
                    <div class="box-body">
                        <div class="form-group">
                            <label>Id</label>
                            <?= $form->render('id') ?>
                        </div>
                        <div class="form-group">
                            <label>Name</label>
                            <?= $form->render('name') ?>
                        </div>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <?= $this->tag->submitButton(['Search', 'class' => 'btn btn-primary']) ?>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>