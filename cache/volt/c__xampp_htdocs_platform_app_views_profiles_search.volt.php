<section class="content-header">
    <h1>
        <?= ucwords($this->router->getControllerName()) ?>
    </h1>
</section>

<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="box box-default color-palette-box box-body table-responsive no-padding">
        <div class="box-body">
            <?= $this->getContent() ?>

            <ul class="pager">
                <li class="previous pull-left">
                    <?= $this->tag->linkTo(['profiles/index', '&larr; Go Back']) ?>
                </li>
                <li class="pull-right">
                    <?= $this->tag->linkTo(['profiles/create', 'Create profiles', 'class' => 'btn']) ?>
                </li>
            </ul>

            <?php $v16012132461iterated = false; ?><?php $v16012132461iterator = $page->items; $v16012132461incr = 0; $v16012132461loop = new stdClass(); $v16012132461loop->self = &$v16012132461loop; $v16012132461loop->length = count($v16012132461iterator); $v16012132461loop->index = 1; $v16012132461loop->index0 = 1; $v16012132461loop->revindex = $v16012132461loop->length; $v16012132461loop->revindex0 = $v16012132461loop->length - 1; ?><?php foreach ($v16012132461iterator as $profile) { ?><?php $v16012132461loop->first = ($v16012132461incr == 0); $v16012132461loop->index = $v16012132461incr + 1; $v16012132461loop->index0 = $v16012132461incr; $v16012132461loop->revindex = $v16012132461loop->length - $v16012132461incr; $v16012132461loop->revindex0 = $v16012132461loop->length - ($v16012132461incr + 1); $v16012132461loop->last = ($v16012132461incr == ($v16012132461loop->length - 1)); ?><?php $v16012132461iterated = true; ?>
            <?php if ($v16012132461loop->first) { ?>
            <table class="table table-bordered table-striped" align="center">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Active?</th>
                    </tr>
                </thead>
                <tbody>
                    <?php } ?>
                    <tr>
                        <td><?= $profile->id ?></td>
                        <td><?= $profile->name ?></td>
                        <td><?= ($profile->active == 'Y' ? 'Yes' : 'No') ?></td>
                        <td width="12%"><?= $this->tag->linkTo(['profiles/edit/' . $profile->id, '<i class="fa fa-pencil"></i> Edit', 'class' => 'btn btn-default']) ?></td>
                        <td width="12%"><?= $this->tag->linkTo(['profiles/delete/' . $profile->id, '<i class="fa fa-remove"></i> Delete', 'class' => 'btn btn-default', 'onClick' => 'return confirm("Are you sure?");']) ?></td>
                    </tr>
                    <?php if ($v16012132461loop->last) { ?>
                </tbody>
                <tr>
                    <td><?= $page->current ?> / <?= $page->total_pages ?></td>
                    <td colspan="10" align="right">
                        <div class="btn-group">
                            <?= $this->tag->linkTo(['profiles/search', '<i class="fa fa-fast-backward"></i> First', 'class' => 'btn btn-default']) ?>
                            <?= $this->tag->linkTo(['profiles/search?page=' . $page->before, '<i class="fa fa-step-backward"></i> Previous', 'class' => 'btn btn-default']) ?>
                            <?= $this->tag->linkTo(['profiles/search?page=' . $page->next, '<i class="fa fa-step-forward"></i> Next', 'class' => 'btn btn-default']) ?>
                            <?= $this->tag->linkTo(['profiles/search?page=' . $page->last, '<i class="fa fa-fast-forward"></i> Last', 'class' => 'btn btn-default']) ?>
                            <!-- <span class="help-inline"><?= $page->current ?>/<?= $page->total_pages ?></span> -->
                        </div>
                    </td>
                </tr>
            </table>
            <?php } ?>
            <?php $v16012132461incr++; } if (!$v16012132461iterated) { ?>
            No profiles are recorded
            <?php } ?>
        </div>
    </div>
</section>