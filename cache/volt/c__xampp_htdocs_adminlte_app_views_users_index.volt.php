<section class="content-header">
    <h1>
        <?= ucwords($this->router->getControllerName()) ?>
    </h1>
</section>

<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="box box-default color-palette-box">
        <div class="box-body">
            <?= $this->getContent() ?>

            <div align="right">
                <?= $this->tag->linkTo(['users/create', '<i class=\'icon-plus-sign\'></i> Create Users', 'class' => 'btn btn-primary']) ?>
            </div>

            <div class="col-md-6">
                <form method="post" action="<?= $this->url->get('users/search') ?>" autocomplete="off">

                    <div class="box-body">
                        <div class="form-group">
                            <label for="id">Id</label>
                            <?= $form->render('id') ?>
                        </div>

                        <div class="form-group">
                            <label for="name">Name</label>
                            <?= $form->render('name') ?>
                        </div>

                        <div class="form-group">
                            <label for="email">E-Mail</label>
                            <?= $form->render('email') ?>
                        </div>

                        <div class="form-group">
                            <label for="profilesId">Profile</label>
                            <?= $form->render('profilesId') ?>
                        </div>

                        <div class="clearfix">
                            <?= $this->tag->submitButton(['Search', 'class' => 'btn btn-primary']) ?>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</section>