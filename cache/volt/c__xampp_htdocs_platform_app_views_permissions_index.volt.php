<section class="content-header">
	<h1>
		<?= ucwords($this->router->getControllerName()) ?>
	</h1>
</section>

<section class="content">
	<!-- Small boxes (Stat box) -->
	<div class="box box-default color-palette-box">
		<div class="box-body">
			<?= $this->getContent() ?>
			<form method="post">
				<div class="col-md-12">
					<div class="row">
						<div class="form-group">
							<label class="col-md-1 control-label">Profile</label>
							<div class="col-md-3">
								<?= $this->tag->select(['profileId', $profiles, 'class' => 'form-control', 'using' => ['id', 'name'], 'useEmpty' => true, 'emptyText' => '...', 'emptyValue' => '']) ?>
							</div>
							<div class="col-md-3">
								<?= $this->tag->submitButton(['Search', 'class' => 'btn btn-primary', 'name' => 'search']) ?>
							</div>
						</div>
					</div>

					<?php if ($this->request->isPost() && $profile) { ?>

					<?php foreach ($this->acl->getResources() as $resource => $actions) { ?>

					<h3><?= $resource ?></h3>

					<table class="table table-bordered table-striped" align="center">
						<thead>
							<tr>
								<th width="5%"></th>
								<th>Description</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($actions as $action) { ?>
							<tr>
								<td align="center"><input type="checkbox" name="permissions[]" value="<?= $resource . '.' . $action ?>"  <?php if (isset($permissions[$resource . '.' . $action])) { ?> checked="checked" <?php } ?>></td>
								<td><?= $this->acl->getActionDescription($action) . ' ' . $resource ?></td>
							</tr>
							<?php } ?>
						</tbody>
					</table>

					<?php } ?>

					<?= $this->tag->submitButton(['Submit', 'class' => 'btn btn-primary', 'name' => 'submit']) ?>   

					<?php } ?>
				</div>
			</form>
		</div>
	</div>
</section>
