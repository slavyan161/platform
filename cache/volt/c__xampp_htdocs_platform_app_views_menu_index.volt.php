<section class="content-header">
    <h1>
        <?= ucwords($this->router->getControllerName()) ?>
    </h1>
</section>

<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="box box-default color-palette-box">
        <div class="box-body">
            <?= $this->getContent() ?>

            <ul class="pager">
                <li class="pull-right">
                    <?= $this->tag->linkTo(['menu/create', 'Create menu', 'class' => 'btn']) ?>
                </li>
            </ul>

            <?php $v423181611iterated = false; ?><?php $v423181611iterator = $page->items; $v423181611incr = 0; $v423181611loop = new stdClass(); $v423181611loop->self = &$v423181611loop; $v423181611loop->length = count($v423181611iterator); $v423181611loop->index = 1; $v423181611loop->index0 = 1; $v423181611loop->revindex = $v423181611loop->length; $v423181611loop->revindex0 = $v423181611loop->length - 1; ?><?php foreach ($v423181611iterator as $menu) { ?><?php $v423181611loop->first = ($v423181611incr == 0); $v423181611loop->index = $v423181611incr + 1; $v423181611loop->index0 = $v423181611incr; $v423181611loop->revindex = $v423181611loop->length - $v423181611incr; $v423181611loop->revindex0 = $v423181611loop->length - ($v423181611incr + 1); $v423181611loop->last = ($v423181611incr == ($v423181611loop->length - 1)); ?><?php $v423181611iterated = true; ?>
            <?php if ($v423181611loop->first) { ?>
            <table class="table table-bordered table-striped box-body table-responsive no-padding" align="center">
                <thead>
                    <tr align="center">
                        <th><center>Id</center></th>
                        <th><center>Parent Id</center></th>
                        <th><center>Name</center></th>
                        <th><center>Icon</center></th>
                        <th><center>Link</center></th>
                    </tr>
                </thead>
                <tbody>
                    <?php } ?>
                    <tr align="center">
                        <td><?= $menu->id ?></td>
                        <td><?= $menu->parent ?></td>
                        <td><?= $menu->name ?></td>
                        <td><i class="<?= ($menu->icon != null ? $menu->icon : 'fa fa-circle-o') ?>"></i></td>
                        <td><?= $menu->link ?></td>
                        <?php if ($menu->link == '#') { ?>
                        <td width="12%"><?= $this->tag->linkTo(['menu/action/' . $menu->id, '<i class="fa fa-plus"></i> Action Menu', 'class' => 'btn btn-default disabled']) ?></td>
                        <?php } else { ?>
                        <td width="12%"><?= $this->tag->linkTo(['menu/action/' . $menu->id, '<i class="fa fa-plus"></i> Action Menu', 'class' => 'btn btn-default']) ?></td>
                        <?php } ?>
                        <td width="12%"><?= $this->tag->linkTo(['menu/edit/' . $menu->id, '<i class="fa fa-pencil"></i> Edit', 'class' => 'btn btn-default']) ?></td>
                        <td width="12%"><?= $this->tag->linkTo(['menu/delete/' . $menu->id, '<i class="fa fa-remove"></i> Delete', 'class' => 'btn btn-default', 'onClick' => 'return confirm("Are you sure?");']) ?></td>
                    </tr>
                    <?php if ($v423181611loop->last) { ?>
                </tbody>
                <tr>
                    <td><?= $page->current ?> / <?= $page->total_pages ?></td>
                    <td colspan="10" align="right">
                        <div class="btn-group">
                            <?= $this->tag->linkTo(['menu/index', '<i class="fa fa-fast-backward"></i> First', 'class' => 'btn btn-default']) ?>
                            <?= $this->tag->linkTo(['menu/index?page=' . $page->before, '<i class="fa fa-step-backward"></i> Previous', 'class' => 'btn btn-default']) ?>
                            <?= $this->tag->linkTo(['menu/index?page=' . $page->next, '<i class="fa fa-step-forward"></i> Next', 'class' => 'btn btn-default']) ?>
                            <?= $this->tag->linkTo(['menu/index?page=' . $page->last, '<i class="fa fa-fast-forward"></i> Last', 'class' => 'btn btn-default']) ?>
                            <!-- <span class="help-inline"><?= $page->current ?>/<?= $page->total_pages ?></span> -->
                        </div>
                    </td>
                </tr>
            </table>
            <?php } ?>
            <?php $v423181611incr++; } if (!$v423181611iterated) { ?>
            No profiles are recorded
            <?php } ?>
        </div>
    </div>
</section>