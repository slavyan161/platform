<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h4 class="page-header">Merchant Management</h4>
		</div>
	</div>
	<div class="col-md-12 row">
		<form class="form-horizontal form-bordered" action="javascript:ctrlProduct.show(this);" method="post">
			<div class="form-group">
				<label class="col-md-3 control-label">Extension (Kode Kota, etc)</label>
				<div class="col-md-2">
					<input class="form-control" name="ext" id="ext" type="text">
				</div>
				<label class="col-md-1 control-label">&nbsp;</label>
				<label class="checkbox-inline">
					<input type="checkbox" id="balance" name="balance">Check Balance
				</label>
				<label class="checkbox-inline">
					<input type="checkbox" id="minus" name="minus">Just Minus Only
				</label>
				<label class="checkbox-inline">
					<input type="checkbox" id="account" name="account">O2W Account
				</label>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label">Date</label>
				<div class="col-md-4">
					<div class="input-daterange input-group" data-plugin-datepicker>
						<span class="input-group-addon">
							<i class="fa fa-calendar"></i>
						</span>
						<input type="date" class="form-control" name="start" id="start">
						<span class="input-group-addon">to</span>
						<input type="date" class="form-control" name="end" id="end">
					</div>
				</div>
				<label class="col-md-1 control-label">Filter</label>
				<div class="col-md-2">
					<select name="sFilter" id="sFilter" class="form-control">
						<option value='all'> All </option>;<?php foreach ($agentArr as $key => $value) { ?><?php if (($value['CPM_A_NAME'] != null)) { ?>
						<option value='<?= $value['CSC_TAU_AREACODE'] ?>'><?= $value['CPM_A_NAME'] ?></option>
						<?php } ?><?php } ?></select>
				</div>
				<button type="submit" class="btn"><span class="fa fa-search"></span> Show</button>
			</div>
		</form>
	</div>
	<hr>
	<div class="col-md-12">
		<div class="row">

			<table class="table table-no-more table-bordered table-striped mb-none" id="tabel">
				<thead>
					<th>No</th>
					<th>Aksi</th>
					<th>PPID</th>
					<th>Nama Loket</th>
					<th id="hServer">Saldo @O2W-SERVER</th>
					<th id="hSaldo">Saldo Real</th>
					<th>Keterangan</th>
					<th>Terdaftar</th>
					<th>Aktivitas Terakhir</th>
					<th>Status</th>
					<th id="hDeposit">Deposit Top-up</th>
				</thead>
				<tbody>
					<?php foreach (range(1, 10) as $i) { ?>
					<tr>	
						<td id="no"><?= $i ?></td>
						<td id="aksi"><?= $i ?></td>
						<td id="ppid"><?= $i ?></td>
						<td id="loket"><?= $i ?></td>
						<td id="server"><?= $i ?></td>
						<td id="saldo"><?= $i ?></td>
						<td id="ket"><?= $i ?></td>
						<td id="terdaftar"><?= $i ?></td>
						<td id="aktivitas"><?= $i ?></td>
						<td id="status"><?= $i ?></td>
						<td id="deposit"><?= $i ?></td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
