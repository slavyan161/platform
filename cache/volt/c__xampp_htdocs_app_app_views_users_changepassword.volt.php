<section class="content-header">
    <h1>
        <?= ucwords($this->router->getControllerName()) ?>
    </h1>
</section>
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="box box-default color-palette-box">
        <div class="box-body">
            <?= $this->getContent() ?>

            <form method="post" autocomplete="off" action="<?= $this->url->get('users/changePassword') ?>">

                <div class="col-md-6">
                    <div class="box-body">

                        <h2>Change Password</h2>

                        <div class="form-group">
                            <label for="password">Password</label>
                            <?= $form->render('password') ?>
                        </div>

                        <div class="form-group">
                            <label for="confirmPassword">Confirm Password</label>
                            <?= $form->render('confirmPassword') ?>
                        </div>

                        <div class="clearfix">
                            <?= $this->tag->submitButton(['Change Password', 'class' => 'btn btn-primary']) ?>
                        </div>

                    </div>
                </div>

            </form>
        </div>
    </div>
</section>