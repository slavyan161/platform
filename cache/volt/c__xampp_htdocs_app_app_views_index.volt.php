<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>OnPays | <?= ucwords($this->router->getControllerName()) ?></title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.7 -->
	<?= $this->tag->stylesheetLink('assets/bower_components/bootstrap/dist/css/bootstrap.min.css') ?>
	<!-- Font Awesome -->
	<?= $this->tag->stylesheetLink('assets/bower_components/font-awesome/css/font-awesome.min.css') ?>
	<!-- Ionicons -->
	<?= $this->tag->stylesheetLink('assets/bower_components/Ionicons/css/ionicons.min.css') ?>
	<!-- Theme style -->
	<?= $this->tag->stylesheetLink('assets/dist/css/AdminLTE.min.css') ?>
	<!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
	<?= $this->tag->stylesheetLink('assets/dist/css/skins/_all-skins.min.css') ?>
  <!-- iCheck -->
  <?= $this->tag->stylesheetLink('assets/plugins/iCheck/square/blue.css') ?>

  <!-- Add CSS-->
  <?= $this->assets->outputCss('headerCss') ?>
  <?= $this->assets->outputInlineCss('headerCss') ?>
  <!-- Add Js -->
  <?= $this->assets->outputJs('headerJs') ?>
  <?= $this->assets->outputInlineJs('headerJs') ?>

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  	<!--[if lt IE 9]
  	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  	[endif]-->

  	<!-- Google Font -->
  	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini login-page fixed">
  	<?= $this->getContent() ?>
  	<!-- ./wrapper -->

  	<!-- jQuery 3 -->
    <?= $this->tag->javascriptInclude('assets/bower_components/jquery/dist/jquery.min.js') ?>
  	<!-- jQuery UI 1.11.4 -->
    <?= $this->tag->javascriptInclude('assets/bower_components/jquery-ui/jquery-ui.min.js') ?>
  	<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
  	<script>
  	$.widget.bridge('uibutton', $.ui.button);
  	</script>
  	<!-- Bootstrap 3.3.7 -->
    <?= $this->tag->javascriptInclude('assets/bower_components/bootstrap/dist/js/bootstrap.min.js') ?>
  	<!-- AdminLTE App -->
    <?= $this->tag->javascriptInclude('assets/dist/js/adminlte.min.js') ?>
    <?= $this->tag->javascriptInclude('assets/dist/js/demo.js') ?>
    
    <!-- Add Js -->
    <?= $this->assets->outputJs('footerJs') ?>
    <?= $this->assets->outputInlineJs('footerJs') ?>
</body>
</html>