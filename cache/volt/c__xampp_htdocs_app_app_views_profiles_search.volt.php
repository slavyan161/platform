<section class="content-header">
    <h1>
        <?= ucwords($this->router->getControllerName()) ?>
    </h1>
</section>

<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="box box-default color-palette-box box-body table-responsive no-padding">
        <div class="box-body">
            <?= $this->getContent() ?>

            <ul class="pager">
                <li class="previous pull-left">
                    <?= $this->tag->linkTo(['profiles/index', '&larr; Go Back']) ?>
                </li>
                <li class="pull-right">
                    <?= $this->tag->linkTo(['profiles/create', 'Create profiles', 'class' => 'btn']) ?>
                </li>
            </ul>

            <?php $v34732546181iterated = false; ?><?php $v34732546181iterator = $page->items; $v34732546181incr = 0; $v34732546181loop = new stdClass(); $v34732546181loop->self = &$v34732546181loop; $v34732546181loop->length = count($v34732546181iterator); $v34732546181loop->index = 1; $v34732546181loop->index0 = 1; $v34732546181loop->revindex = $v34732546181loop->length; $v34732546181loop->revindex0 = $v34732546181loop->length - 1; ?><?php foreach ($v34732546181iterator as $profile) { ?><?php $v34732546181loop->first = ($v34732546181incr == 0); $v34732546181loop->index = $v34732546181incr + 1; $v34732546181loop->index0 = $v34732546181incr; $v34732546181loop->revindex = $v34732546181loop->length - $v34732546181incr; $v34732546181loop->revindex0 = $v34732546181loop->length - ($v34732546181incr + 1); $v34732546181loop->last = ($v34732546181incr == ($v34732546181loop->length - 1)); ?><?php $v34732546181iterated = true; ?>
            <?php if ($v34732546181loop->first) { ?>
            <table class="table table-bordered table-striped" align="center">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Active?</th>
                    </tr>
                </thead>
                <tbody>
                    <?php } ?>
                    <tr>
                        <td><?= $profile->id ?></td>
                        <td><?= $profile->name ?></td>
                        <td><?= ($profile->active == 'Y' ? 'Yes' : 'No') ?></td>
                        <td width="12%"><?= $this->tag->linkTo(['profiles/edit/' . $profile->id, '<i class="fa fa-pencil"></i> Edit', 'class' => 'btn btn-default']) ?></td>
                        <td width="12%"><?= $this->tag->linkTo(['profiles/delete/' . $profile->id, '<i class="fa fa-remove"></i> Delete', 'class' => 'btn btn-default', 'onClick' => 'return confirm("Are you sure?");']) ?></td>
                    </tr>
                    <?php if ($v34732546181loop->last) { ?>
                </tbody>
                <tr>
                    <td><?= $page->current ?> / <?= $page->total_pages ?></td>
                    <td colspan="10" align="right">
                        <div class="btn-group">
                            <?= $this->tag->linkTo(['profiles/search', '<i class="fa fa-fast-backward"></i> First', 'class' => 'btn btn-default']) ?>
                            <?= $this->tag->linkTo(['profiles/search?page=' . $page->before, '<i class="fa fa-step-backward"></i> Previous', 'class' => 'btn btn-default']) ?>
                            <?= $this->tag->linkTo(['profiles/search?page=' . $page->next, '<i class="fa fa-step-forward"></i> Next', 'class' => 'btn btn-default']) ?>
                            <?= $this->tag->linkTo(['profiles/search?page=' . $page->last, '<i class="fa fa-fast-forward"></i> Last', 'class' => 'btn btn-default']) ?>
                            <!-- <span class="help-inline"><?= $page->current ?>/<?= $page->total_pages ?></span> -->
                        </div>
                    </td>
                </tr>
            </table>
            <?php } ?>
            <?php $v34732546181incr++; } if (!$v34732546181iterated) { ?>
            No profiles are recorded
            <?php } ?>
        </div>
    </div>
</section>