<section class="content-header">
    <h1>
        <?= ucwords($this->router->getControllerName()) ?>
    </h1>
</section>
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="box box-default color-palette-box">
        <div class="box-body">
            <?= $this->getContent() ?>
            <form method="post" autocomplete="off">

                <ul class="pager">
                    <li class="previous pull-left">
                        <?= $this->tag->linkTo(['menu', '&larr; Go Back']) ?>
                    </li>
                    <li class="pull-right">
                        <?= $this->tag->submitButton(['Save', 'class' => 'btn btn-success']) ?>
                    </li>
                </ul>


                <div class="col-md-6">
                    <div class="row">
                        <div class="box-body">
                            <h2>Create a Menu</h2>
                            <div class="form-group">
                                <label>Parent</label>
                                <?= $form->render('parent') ?>
                            </div>
                            <div class="form-group">
                                <label>Name</label>
                                <?= $form->render('name') ?>
                            </div>
                            <div class="form-group">
                                <label>Icon</label>
                                <?= $form->render('icon') ?>
                            </div>
                            <div class="form-group">
                                <label>Link</label>
                                <?= $form->render('link') ?>
                            </div>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>
</section>