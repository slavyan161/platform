<section class="content-header">
    <h1>
        <?= ucwords($this->router->getControllerName()) ?>
    </h1>
</section>

<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="box box-default color-palette-box">
        <div class="box-body">
            <?= $this->getContent() ?>

            <ul class="pager">
                <li class="previous pull-left">
                    <?= $this->tag->linkTo(['partnertype/index', '&larr; Go Back']) ?>
                </li>
                <li class="pull-right">
                    <?= $this->tag->linkTo(['partnertype/create', 'Create Partner', 'class' => 'btn']) ?>
                </li>
            </ul>

            <?php $v13667160901iterated = false; ?><?php $v13667160901iterator = $page->items; $v13667160901incr = 0; $v13667160901loop = new stdClass(); $v13667160901loop->self = &$v13667160901loop; $v13667160901loop->length = count($v13667160901iterator); $v13667160901loop->index = 1; $v13667160901loop->index0 = 1; $v13667160901loop->revindex = $v13667160901loop->length; $v13667160901loop->revindex0 = $v13667160901loop->length - 1; ?><?php foreach ($v13667160901iterator as $partner) { ?><?php $v13667160901loop->first = ($v13667160901incr == 0); $v13667160901loop->index = $v13667160901incr + 1; $v13667160901loop->index0 = $v13667160901incr; $v13667160901loop->revindex = $v13667160901loop->length - $v13667160901incr; $v13667160901loop->revindex0 = $v13667160901loop->length - ($v13667160901incr + 1); $v13667160901loop->last = ($v13667160901incr == ($v13667160901loop->length - 1)); ?><?php $v13667160901iterated = true; ?>
            <?php if ($v13667160901loop->first) { ?>
            <table class="table table-bordered table-striped" align="center">
                <thead>
                    <tr>
                        <th width="5%">Id</th>
                        <th>Name</th>
                        <th colspan="2">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php } ?>
                    <tr>
                        <td><?= $partner->id ?></td>
                        <td><?= $partner->name ?></td>
                        <td width="12%"><?= $this->tag->linkTo(['partnertype/edit/' . $partner->id, '<i class="fa fa-pencil"></i> Edit', 'class' => 'btn btn-default']) ?></td>
                        <td width="12%"><?= $this->tag->linkTo(['partnertype/delete/' . $partner->id, '<i class="fa fa-remove"></i> Delete', 'class' => 'btn btn-default', 'onClick' => 'return confirm("Are you sure?");']) ?></td>
                    </tr>
                    <?php if ($v13667160901loop->last) { ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="10" align="right">
                            <div class="btn-group">
                                <?= $this->tag->linkTo(['partnertype/index', '<i class="fa fa-fast-backward"></i> First', 'class' => 'btn btn-default']) ?>
                                <?= $this->tag->linkTo(['partnertype/index?page=' . $page->before, '<i class="fa fa-step-backward"></i> Previous', 'class' => 'btn btn-default']) ?>
                                <?= $this->tag->linkTo(['partnertype/index?page=' . $page->next, '<i class="fa fa-step-forward"></i> Next', 'class' => 'btn btn-default']) ?>
                                <?= $this->tag->linkTo(['partnertype/index?page=' . $page->last, '<i class="fa fa-fast-forward"></i> Last', 'class' => 'btn btn-default']) ?>
                                 <!-- <span class="help-inline"><?= $page->current ?>/<?= $page->total_pages ?></span>  -->
                            </div>
                        </td>
                    </tr>
                </tfoot>
            </table>
            <?php } ?>
            <?php $v13667160901incr++; } if (!$v13667160901iterated) { ?>
            No profiles are recorded
            <?php } ?>
        </div>
    </div>
</section>

