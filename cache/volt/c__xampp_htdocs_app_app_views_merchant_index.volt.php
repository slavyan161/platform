<section class="content-header">
	<h1>
		<?= ucwords($this->router->getControllerName()) ?>
		<!-- Trx Monitoring -->
	</h1>
</section>
<section class="content">
	<!-- Small boxes (Stat box) -->
	<div class="box box-default color-palette-box">
		<div class="box-body">
			<?= $this->getContent() ?>
			<div class="col-md-12 row">
				<form class="form-horizontal form-bordered" action="javascript:ctrlProduct.show(this);" method="post">
					<div class="form-group">
						<label class="col-md-3 control-label">Extension (Kode Kota, etc)</label>
						<div class="col-md-2">
							<input class="form-control" name="ext" id="ext" type="text">
						</div>
						<label class="col-md-1 control-label">&nbsp;</label>
						<label class="checkbox-inline">
							<input type="checkbox" id="balance" name="balance">Check Balance
						</label>
						<label class="checkbox-inline">
							<input type="checkbox" id="minus" name="minus">Just Minus Only
						</label>
						<label class="checkbox-inline">
							<input type="checkbox" id="account" name="account">O2W Account
						</label>
					</div>
					<div class="form-group">
						<label class="col-md-2 control-label">Date</label>
						<div class="col-md-4">
							<div class="input-daterange input-group" data-plugin-datepicker>
								<span class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</span>
								<input type="date" class="form-control" name="start" id="start">
								<span class="input-group-addon">to</span>
								<input type="date" class="form-control" name="end" id="end">
							</div>
						</div>
						<label class="col-md-1 control-label">Filter</label>
						<div class="col-md-2">
							<select name="sFilter" id="sFilter" class="form-control">
								<option value='all'> All </option>;<?php foreach ($agentArr as $key => $value) { ?><?php if (($value['CPM_A_NAME'] != null)) { ?>
								<option value='<?= $value['CSC_TAU_AREACODE'] ?>'><?= $value['CPM_A_NAME'] ?></option>
								<?php } ?><?php } ?></select>
						</div>
						<button type="submit" class="btn"><span class="fa fa-search"></span> Show</button>
					</div>
				</form>
			</div>
			<hr>
			<div class="col-md-12">
				<div class="row">

					<table class="table table-no-more table-bordered table-striped mb-none" id="tabel">
						<thead>
							<th>No</th>
							<th>Aksi</th>
							<th>PPID</th>
							<th>Nama Loket</th>
							<th id="hServer">Saldo Efektif</th>
							<th id="hSaldo" hidden="">Saldo Real</th>
							<th>Keterangan</th>
							<th>Terdaftar</th>
							<th>Aktivitas Terakhir</th>
							<th>Status</th>
							<th id="hDeposit" hidden="">Deposit Top-up</th>
						</thead>
						<tbody id="tbody">

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
