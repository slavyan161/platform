<section class="content-header">
    <h1>
        <?= ucwords($this->router->getControllerName()) ?>
    </h1>
</section>

<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="box box-default color-palette-box">
        <div class="box-body">
            <?= $this->getContent() ?>
            <form method="post">
            <div class="col-md-3">
            <?= $this->tag->select(['partnersId', $partnerType, 'class' => 'form-control', 'using' => ['id', 'name'], 'useEmpty' => false, 'emptyText' => '...', 'emptyValue' => '']) ?>
            </div>
            <div class="col-md-3">
                <?= $this->tag->submitButton(['Search', 'class' => 'btn btn-primary', 'name' => 'search']) ?>
            </div>
            <div class="col-md-3 pull-right">
               <?= $this->tag->linkTo(['partner/create', 'Create Partner', 'class' => 'btn btn-primary']) ?>
            </div>
            <br>
            <?php if ($this->request->isPost()) { ?>
            <br>
            <?php $i = 0; ?>
            <?php $v34297317961iterated = false; ?><?php $v34297317961iterator = $partners; $v34297317961incr = 0; $v34297317961loop = new stdClass(); $v34297317961loop->self = &$v34297317961loop; $v34297317961loop->length = count($v34297317961iterator); $v34297317961loop->index = 1; $v34297317961loop->index0 = 1; $v34297317961loop->revindex = $v34297317961loop->length; $v34297317961loop->revindex0 = $v34297317961loop->length - 1; ?><?php foreach ($v34297317961iterator as $partner) { ?><?php $v34297317961loop->first = ($v34297317961incr == 0); $v34297317961loop->index = $v34297317961incr + 1; $v34297317961loop->index0 = $v34297317961incr; $v34297317961loop->revindex = $v34297317961loop->length - $v34297317961incr; $v34297317961loop->revindex0 = $v34297317961loop->length - ($v34297317961incr + 1); $v34297317961loop->last = ($v34297317961incr == ($v34297317961loop->length - 1)); ?><?php $v34297317961iterated = true; ?>
            <?php if ($v34297317961loop->first) { ?>
            <table class="table table-bordered table-striped" align="center">
                <thead>
                    <tr>
                        <th width="5%">No</th>
                        <!-- <th width="5%">Id</th> -->
                        <th>Partner Name</th>
                        <th>Partner Type</th>
                        <th colspan="2">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php } ?>
                    <tr>
                        <?php $i = $i + 1; ?>
                        <td><?= $i ?></td>
                        <!-- <td><?= $partner->partnerId ?></td> -->
                        <td><?= $partner->partnerName ?></td>
                        <td><?= $partner->partnerTypeName ?></td>
                        <td width="12%"><?= $this->tag->linkTo(['partner/edit/' . $partner->partnerId, '<i class="fa fa-pencil"></i> Edit', 'class' => 'btn btn-default']) ?></td>
                        <td width="12%"><?= $this->tag->linkTo(['partner/delete/' . $partner->partnerId, '<i class="fa fa-remove"></i> Delete', 'class' => 'btn btn-default', 'onClick' => 'return confirm("Are you sure?");']) ?></td>
                    </tr>
                    <?php if ($v34297317961loop->last) { ?>
                </tbody>
            </table>
            <?php } ?>
            <?php $v34297317961incr++; } if (!$v34297317961iterated) { ?>
            No profiles are recorded
            <?php } ?>
        </div>
        <?php } ?>
        </form>
    </div>
</section>

