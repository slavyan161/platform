<section class="content-header">
    <h1>
        <?= ucwords($this->router->getControllerName()) ?>
    </h1>
</section>
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="box box-default color-palette-box">
        <div class="box-body">
            <?= $this->getContent() ?>
            <form method="post" autocomplete="off">

                <ul class="pager">
                    <li class="previous pull-left">
                        <?= $this->tag->linkTo(['users', '&larr; Go Back']) ?>
                    </li>
                    <li class="pull-right">
                        <?= $this->tag->submitButton(['Save', 'class' => 'btn btn-success']) ?>
                    </li>
                </ul>


                <div class="col-md-6">
                    <div class="box-body">
                        <h2>Create a User</h2>

                        <div class="form-group">
                            <label for="name">Name</label>
                            <?= $form->render('name') ?>
                        </div>

                        <div class="form-group">
                            <label for="email">E-Mail</label>
                            <?= $form->render('email') ?>
                        </div>

                        <div class="form-group">
                            <label for="profilesId">Profile</label>
                            <?= $form->render('profilesId') ?>
                        </div>

                    </div>
                </div>

            </form>
        
        </div>
    </div>
</section>