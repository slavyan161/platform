<section class="content-header">
    <h1>
        <?= ucwords($this->router->getControllerName()) ?>
    </h1>
</section>
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="box box-default color-palette-box">
        <div class="box-body">
            <?= $this->getContent() ?>
            <form method="post" autocomplete="off">

                <ul class="pager">
                    <li class="previous pull-left">
                        <?= $this->tag->linkTo(['profiles', '&larr; Go Back']) ?>
                    </li>
                    <li class="pull-right">
                        <?= $this->tag->submitButton(['Save', 'class' => 'btn btn-success']) ?>
                    </li>
                </ul>


                <div class="center scaffold">

                    <h2>Edit profile</h2>

                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#A" data-toggle="tab">Basic</a></li>
                        <li><a href="#B" data-toggle="tab">Users</a></li>
                    </ul>

                    <div class="tabbable">
                        <div class="tab-content">
                            <div class="tab-pane active" id="A">

                                <div class="box-body">
                                    <?= $form->render('id') ?>

                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <?= $form->render('name') ?>
                                    </div>

                                    <div class="form-group">
                                        <label for="active">Active?</label>
                                        <?= $form->render('active') ?>
                                    </div>
                                </div>

                            </div>

                            <div class="tab-pane" id="B">
                                <p>
                                    <table class="table table-bordered table-striped" align="center">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Name</th>
                                                <th>Banned?</th>
                                                <th>Suspended?</th>
                                                <th>Active?</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $v23914005991iterated = false; ?><?php foreach ($profile->users as $user) { ?><?php $v23914005991iterated = true; ?>
                                            <tr>
                                                <td><?= $user->id ?></td>
                                                <td><?= $user->name ?></td>
                                                <td><?= ($user->banned == 'Y' ? 'Yes' : 'No') ?></td>
                                                <td><?= ($user->suspended == 'Y' ? 'Yes' : 'No') ?></td>
                                                <td><?= ($user->active == 'Y' ? 'Yes' : 'No') ?></td>
                                                <td width="12%"><?= $this->tag->linkTo(['users/edit/' . $user->id, '<i class="fa fa-pencil"></i> Edit', 'class' => 'btn btn-default']) ?></td>
                                                <td width="12%"><?= $this->tag->linkTo(['users/delete/' . $user->id, '<i class="fa fa-remove"></i> Delete', 'class' => 'btn btn-default']) ?></td>
                                            </tr>
                                            <?php } if (!$v23914005991iterated) { ?>
                                            <tr><td colspan="3" align="center">There are no users assigned to this profile</td></tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </p>
                            </div>

                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>
</section>