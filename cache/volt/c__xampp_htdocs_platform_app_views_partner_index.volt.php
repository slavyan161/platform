<section class="content-header">
    <h1>
        <?= ucwords($this->router->getControllerName()) ?>
    </h1>
</section>

<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="box box-default color-palette-box">
        <div class="box-body">
            <?= $this->getContent() ?>
            <form method="post">
            <div class="col-md-3">
            <?= $this->tag->select(['partnersId', $partnerType, 'class' => 'form-control', 'using' => ['id', 'name'], 'useEmpty' => false, 'emptyText' => '...', 'emptyValue' => '']) ?>
            </div>
            <div class="col-md-3">
                <?= $this->tag->submitButton(['Search', 'class' => 'btn btn-primary', 'name' => 'search']) ?>
            </div>
            <div class="col-md-3 pull-right">
               <?= $this->tag->linkTo(['partner/create', 'Create Partner', 'class' => 'btn btn-primary']) ?>
            </div>
            <br>
            <?php if ($this->request->isPost()) { ?>
            <br>
            <?php $i = 0; ?>
            <?php $v42877950001iterated = false; ?><?php $v42877950001iterator = $partners; $v42877950001incr = 0; $v42877950001loop = new stdClass(); $v42877950001loop->self = &$v42877950001loop; $v42877950001loop->length = count($v42877950001iterator); $v42877950001loop->index = 1; $v42877950001loop->index0 = 1; $v42877950001loop->revindex = $v42877950001loop->length; $v42877950001loop->revindex0 = $v42877950001loop->length - 1; ?><?php foreach ($v42877950001iterator as $partner) { ?><?php $v42877950001loop->first = ($v42877950001incr == 0); $v42877950001loop->index = $v42877950001incr + 1; $v42877950001loop->index0 = $v42877950001incr; $v42877950001loop->revindex = $v42877950001loop->length - $v42877950001incr; $v42877950001loop->revindex0 = $v42877950001loop->length - ($v42877950001incr + 1); $v42877950001loop->last = ($v42877950001incr == ($v42877950001loop->length - 1)); ?><?php $v42877950001iterated = true; ?>
            <?php if ($v42877950001loop->first) { ?>
            <table class="table table-bordered table-striped" align="center">
                <thead>
                    <tr>
                        <th width="5%">No</th>
                        <!-- <th width="5%">Id</th> -->
                        <th>Partner Name</th>
                        <th>Partner Type</th>
                        <th colspan="2">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php } ?>
                    <tr>
                        <?php $i = $i + 1; ?>
                        <td><?= $i ?></td>
                        <!-- <td><?= $partner->partnerId ?></td> -->
                        <td><?= $partner->partnerName ?></td>
                        <td><?= $partner->partnerTypeName ?></td>
                        <td width="12%"><?= $this->tag->linkTo(['partner/edit/' . $partner->partnerId, '<i class="fa fa-pencil"></i> Edit', 'class' => 'btn btn-default']) ?></td>
                        <td width="12%"><?= $this->tag->linkTo(['partner/delete/' . $partner->partnerId, '<i class="fa fa-remove"></i> Delete', 'class' => 'btn btn-default', 'onClick' => 'return confirm("Are you sure?");']) ?></td>
                    </tr>
                    <?php if ($v42877950001loop->last) { ?>
                </tbody>
            </table>
            <?php } ?>
            <?php $v42877950001incr++; } if (!$v42877950001iterated) { ?>
            No profiles are recorded
            <?php } ?>
        </div>
        <?php } ?>
        </form>
    </div>
</section>

