<section class="content-header">
    <h1>
        <?= ucwords($this->router->getControllerName()) ?>
    </h1>
</section>

<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="box box-default color-palette-box">
        <div class="box-body">
            <?= $this->getContent() ?>

            <ul class="pager">
                <li class="previous pull-left">
                    <?= $this->tag->linkTo(['profiles/index', '&larr; Go Back']) ?>
                </li>
                <li class="pull-right">
                    <?= $this->tag->linkTo(['profiles/create', 'Create profiles', 'class' => 'btn']) ?>
                </li>
            </ul>

            <?php $v26660885831iterated = false; ?><?php $v26660885831iterator = $page->items; $v26660885831incr = 0; $v26660885831loop = new stdClass(); $v26660885831loop->self = &$v26660885831loop; $v26660885831loop->length = count($v26660885831iterator); $v26660885831loop->index = 1; $v26660885831loop->index0 = 1; $v26660885831loop->revindex = $v26660885831loop->length; $v26660885831loop->revindex0 = $v26660885831loop->length - 1; ?><?php foreach ($v26660885831iterator as $profile) { ?><?php $v26660885831loop->first = ($v26660885831incr == 0); $v26660885831loop->index = $v26660885831incr + 1; $v26660885831loop->index0 = $v26660885831incr; $v26660885831loop->revindex = $v26660885831loop->length - $v26660885831incr; $v26660885831loop->revindex0 = $v26660885831loop->length - ($v26660885831incr + 1); $v26660885831loop->last = ($v26660885831incr == ($v26660885831loop->length - 1)); ?><?php $v26660885831iterated = true; ?>
            <?php if ($v26660885831loop->first) { ?>
            <table class="table table-bordered table-striped" align="center">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Active?</th>
                    </tr>
                </thead>
                <tbody>
                    <?php } ?>
                    <tr>
                        <td><?= $profile->id ?></td>
                        <td><?= $profile->name ?></td>
                        <td><?= ($profile->active == 'Y' ? 'Yes' : 'No') ?></td>
                        <td width="12%"><?= $this->tag->linkTo(['profiles/edit/' . $profile->id, '<i class="fa fa-pencil"></i> Edit', 'class' => 'btn btn-default']) ?></td>
                        <td width="12%"><?= $this->tag->linkTo(['profiles/delete/' . $profile->id, '<i class="fa fa-remove"></i> Delete', 'class' => 'btn btn-default']) ?></td>
                    </tr>
                    <?php if ($v26660885831loop->last) { ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="10" align="right">
                            <div class="btn-group">
                                <?= $this->tag->linkTo(['profiles/search', '<i class="fa fa-fast-backward"></i> First', 'class' => 'btn btn-default']) ?>
                                <?= $this->tag->linkTo(['profiles/search?page=' . $page->before, '<i class="fa fa-step-backward"></i> Previous', 'class' => 'btn btn-default']) ?>
                                <?= $this->tag->linkTo(['profiles/search?page=' . $page->next, '<i class="fa fa-step-forward"></i> Next', 'class' => 'btn btn-default']) ?>
                                <?= $this->tag->linkTo(['profiles/search?page=' . $page->last, '<i class="fa fa-fast-forward"></i> Last', 'class' => 'btn btn-default']) ?>
                                <!-- <span class="help-inline"><?= $page->current ?>/<?= $page->total_pages ?></span> -->
                            </div>
                        </td>
                    </tr>
                </tfoot>
            </table>
            <?php } ?>
            <?php $v26660885831incr++; } if (!$v26660885831iterated) { ?>
            No profiles are recorded
            <?php } ?>
        </div>
    </div>
</section>