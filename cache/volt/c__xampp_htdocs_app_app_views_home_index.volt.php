<section class="content-header">
	<h1>
		<?= ucwords($this->router->getControllerName()) ?>
		<!-- Trx Monitoring -->
	</h1>
</section>
<section class="content">
	<!-- Small boxes (Stat box) -->
	<div class="box box-default color-palette-box">
		<div class="box-body">
			<?= $this->getContent() ?>
			<div class="col-lg-12">
				<div class="col-md-3 row">
					<b>Product :</b> 
					<select name="monActive" id="monActive" onChange="ctrlProduct.change()"><?php foreach ($arrRole as $key => $value) { ?><?php if ($value['CSC_TRU_ROLECODE'] == 'admin') { ?>
						<option value='all'> All Product </option><?php foreach ($arrTrxModule as $keys => $vals) { ?><option value='<?= $vals['CSC_TMU_TRXCODE'] ?>'><?= $vals['CSC_TD_ALIASNAME'] ?></option><?php } ?><?php } ?><?php } ?></select>
				</div>
				<div class="col-md-3">
					<b>Filter :</b> 
					<select name="sFilter" id="sFilter" onChange="ctrlProduct.change()">
						<option value='all'> All </option><?php foreach ($agentArr as $key => $value) { ?><?php if ($value['CPM_A_NAME'] != null) { ?>
						<option value='<?= $value['CSC_TAU_AREACODE'] ?>'><?= $value['CPM_A_NAME'] ?></option>
						<?php } ?><?php } ?></select>
				</div>
			</div>
			<hr>
			<div class="col-md-12">
				<b>CATATAN</b>: INFORMASI YANG TERTERA PADA WEBSITE INI <b>BELUM</b> MERUPAKAN NILAI TRANSAKSI AKHIR <br />KARENA PADA AKHIR TRANSAKSI AKAN DILAKUKAN NORMALISASI DAN VERIFIKASI TERHADAP DATA TRANSAKSI YANG MASUK.<br />NILAI TRANSAKSI AKHIR DAPAT DIKONFIRMASIKAN KE ONPAYS CALL CENTRE (1 JAM SETELAH KAS TUTUP).<br /><br />

				<table class="table" id="tabel">
					<tbody>
						<tr>
							<td width='20%'>Waktu</td>
							<td>: <?= date('d/m/Y H:i:s') . ' ' . $inc['VSI_TIME_ZONE_LABEL'] ?></td>
						</tr>
						<tr>
							<td width='20%'>Product</td>
							<td id="prod">: </td>
						</tr>
						<tr>
							<td width='20%'>Payment Point (aktif)</td>
							<td id="nPP">: </td>
						</tr>
						<tr>
							<td width='20%'>Total Rekening dilayani</td>
							<td id="N_REK">: </td>
						</tr>
						<tr>
							<td width='20%'>Total Bulan Tagihan</td>
							<td id="N_MONTH">: </td>
						</tr>
						<tr>
							<td width='20%'>Total Biaya Tagihan</td>
							<td id="TOTAL_TAG">: </td>
						</tr>
						<tr>
							<td width='20%'>Total Bea Admin</td>
							<td id="TOTAL_ADM">: </td>
						</tr>
						<tr>
							<td width='20%'>Total</td>
							<td id="TOTAL">: </td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</section>