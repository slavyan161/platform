<section class="content-header">
    <h1>
        <?= ucwords($this->router->getControllerName()) ?>
    </h1>
</section>

<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="box box-default color-palette-box">
        <div class="box-body">
            <?= $this->getContent() ?>

            <?php $v36813161461iterated = false; ?><?php $v36813161461iterator = $dataPartner; $v36813161461incr = 0; $v36813161461loop = new stdClass(); $v36813161461loop->self = &$v36813161461loop; $v36813161461loop->length = count($v36813161461iterator); $v36813161461loop->index = 1; $v36813161461loop->index0 = 1; $v36813161461loop->revindex = $v36813161461loop->length; $v36813161461loop->revindex0 = $v36813161461loop->length - 1; ?><?php foreach ($v36813161461iterator as $partner) { ?><?php $v36813161461loop->first = ($v36813161461incr == 0); $v36813161461loop->index = $v36813161461incr + 1; $v36813161461loop->index0 = $v36813161461incr; $v36813161461loop->revindex = $v36813161461loop->length - $v36813161461incr; $v36813161461loop->revindex0 = $v36813161461loop->length - ($v36813161461incr + 1); $v36813161461loop->last = ($v36813161461incr == ($v36813161461loop->length - 1)); ?><?php $v36813161461iterated = true; ?>
            <?php if ($v36813161461loop->first) { ?>
            <table class="table table-bordered table-striped" align="center">
                <thead>
                    <tr>
                        <th width="5%">Partner Id</th>
                        <th>Partner Name</th>
                        <th colspan="2">Partner Type</th>
                    </tr>
                </thead>
                <tbody>
                    <?php } ?>
                    <tr>
                        <td><?= $partner->partnerId ?></td>
                        <td><?= $partner->partnerName ?></td>
                        <td><?= $partner->partnerTypeName ?></td>
                    </tr>
                    <?php if ($v36813161461loop->last) { ?>
                </tbody>
            </table>
            <?php } ?>
            <?php $v36813161461incr++; } if (!$v36813161461iterated) { ?>
            You dont have Permissions to see any Data
            <?php } ?>
        </div>
    </div>
</section>