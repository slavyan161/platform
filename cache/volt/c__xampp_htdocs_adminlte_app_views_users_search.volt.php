<section class="content-header">
    <h1>
        <?= ucwords($this->router->getControllerName()) ?>
    </h1>
</section>

<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="box box-default color-palette-box">
        <div class="box-body">
            <?= $this->getContent() ?>

            <ul class="pager">
                <li class="previous pull-left">
                    <?= $this->tag->linkTo(['users/index', '&larr; Go Back']) ?>
                </li>
                <li class="pull-right">
                    <?= $this->tag->linkTo(['users/create', 'Create users', 'class' => 'btn']) ?>
                </li>
            </ul>

            <?php $v26264842771iterated = false; ?><?php $v26264842771iterator = $page->items; $v26264842771incr = 0; $v26264842771loop = new stdClass(); $v26264842771loop->self = &$v26264842771loop; $v26264842771loop->length = count($v26264842771iterator); $v26264842771loop->index = 1; $v26264842771loop->index0 = 1; $v26264842771loop->revindex = $v26264842771loop->length; $v26264842771loop->revindex0 = $v26264842771loop->length - 1; ?><?php foreach ($v26264842771iterator as $user) { ?><?php $v26264842771loop->first = ($v26264842771incr == 0); $v26264842771loop->index = $v26264842771incr + 1; $v26264842771loop->index0 = $v26264842771incr; $v26264842771loop->revindex = $v26264842771loop->length - $v26264842771incr; $v26264842771loop->revindex0 = $v26264842771loop->length - ($v26264842771incr + 1); $v26264842771loop->last = ($v26264842771incr == ($v26264842771loop->length - 1)); ?><?php $v26264842771iterated = true; ?>
            <?php if ($v26264842771loop->first) { ?>
            <table class="table table-bordered table-striped" align="center">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Profile</th>
                        <th>Banned?</th>
                        <th>Suspended?</th>
                        <th>Confirmed?</th>
                    </tr>
                </thead>
                <tbody>
                    <?php } ?>
                    <tr>
                        <td><?= $user->id ?></td>
                        <td><?= $user->name ?></td>
                        <td><?= $user->email ?></td>
                        <td><?= $user->profile->name ?></td>
                        <td><?= ($user->banned == 'Y' ? 'Yes' : 'No') ?></td>
                        <td><?= ($user->suspended == 'Y' ? 'Yes' : 'No') ?></td>
                        <td><?= ($user->active == 'Y' ? 'Yes' : 'No') ?></td>
                        <td width="12%"><?= $this->tag->linkTo(['users/edit/' . $user->id, '<i class="fa fa-pencil"></i> Edit', 'class' => 'btn btn-default']) ?></td>
                        <td width="12%"><?= $this->tag->linkTo(['users/delete/' . $user->id, '<i class="fa fa-remove"></i> Delete', 'class' => 'btn btn-default']) ?></td>
                    </tr>
                    <?php if ($v26264842771loop->last) { ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="10" align="right">
                            <div class="btn-group">
                                <?= $this->tag->linkTo(['users/search', '<i class="fa fa-fast-backward"></i> First', 'class' => 'btn btn-default']) ?>
                                <?= $this->tag->linkTo(['users/search?page=' . $page->before, '<i class="fa fa-step-backward"></i> Previous', 'class' => 'btn btn-default']) ?>
                                <?= $this->tag->linkTo(['users/search?page=' . $page->next, '<i class="fa fa-step-forward"></i> Next', 'class' => 'btn btn-default']) ?>
                                <?= $this->tag->linkTo(['users/search?page=' . $page->last, '<i class="fa fa-fast-forward"></i> Last', 'class' => 'btn btn-default']) ?>
                                <!-- <span class="help-inline"><?= $page->current ?>/<?= $page->total_pages ?></span> -->
                            </div>
                        </td>
                    </tr>
                </tfoot>
            </table>
            <?php } ?>
            <?php $v26264842771incr++; } if (!$v26264842771iterated) { ?>
            No users are recorded
            <?php } ?>
        </div>
    </div>
</section>