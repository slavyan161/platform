$(function() {
    // if(!getCookie('monActive') || getCookie('monActive')=='null') {
    //     $('#monActive').val('prepaid');
    // } else if(!getCookie('sFilter') || getCookie('sFilter')=='null') {
    //     $('#sFilter').val(getCookie('all'));
    // } else {   
    //     $('#monActive').val(getCookie('monActive'));
    //     $('#sFilter').val(getCookie('sFilter'));
    // }
    // ctrlProduct.show();
    ctrlProduct = {
        ext: '',
        balance: '',
        minus: '',
        account: '',
        start: '',
        end: '',
        sFilter: '',
        show: function() {
            ctrlProduct.ext = $('#ext').val();
            ctrlProduct.balance = $("#balance").is(":checked");
            ctrlProduct.minus = $('#minus').is(":checked");
            ctrlProduct.account = $('#account').is(":checked");
            ctrlProduct.start = $('#start').val();
            ctrlProduct.end = $('#end').val();
            ctrlProduct.sFilter = $('#sFilter').val();
            $.ajax({
                url: 'merchant/filter',
                type: 'POST',
                async: false,
                cache: true,
                data: {
                    'ext': ctrlProduct.ext,
                    'balance': ctrlProduct.balance,
                    'minus': ctrlProduct.minus,
                    'account': ctrlProduct.account,
                    'start': ctrlProduct.start,
                    'end': ctrlProduct.end,
                    'sFilter': ctrlProduct.sFilter
                },
                xhrFields: {
                    withCredentials: false
                },
            //milisecond timeout
            timeout: 0,
            success: function(result) {
                //set cookie
                // setCookie('monActive', ctrlProduct.var_monActive);
                // setCookie('sFilter', ctrlProduct.var_sFilter);
                //
                var bCheckFundReconciled = '';
                var data = '';
                var role = '';
                //
                document.getElementById('tbody').innerHTML = '';
                //
                jsonData = JSON.parse(result);
                for (key in jsonData) {                    
                    if (key === 'bCheckFundReconciled') {
                        bCheckFundReconciled = jsonData[key];
                    } 

                    if (key === 'data') {
                        data = jsonData[key];
                    }

                    if (key === 'role') {
                        role = jsonData[key];
                    }
                }

                if (data.length > 0) {
                    if (!bCheckFundReconciled) {
                        if (ctrlProduct.account) {
                            document.getElementById('hServer').innerHTML = '<th id="hServer">Saldo @O2W-SERVER</th>';
                        } else {
                            document.getElementById('hServer').innerHTML = '<th id="hServer">Saldo Efektif</th>';
                        }

                        if (ctrlProduct.balance) {
                            $('#hSaldo').removeAttr('hidden', '');
                        } else {
                            $('#hSaldo').attr('hidden', '');
                        }

                        if(role.indexOf("maker") != -1) {
                            $('#hDeposit').removeAttr('hidden', '');
                        } else {
                            $('#hDeposit').attr('hidden', '');
                        }

                        for (var i = 0; i < data.length; i++) {
                            document.getElementById('tbody').innerHTML += '<tr>\
                            <td>'+ (i+1) +'</td>\
                            <td>'+ (i+1) +'</td>\
                            <td>'+ (data[i][0]) +'</td>\
                            <td>'+ (data[i][7]) +'</td>\
                            <td>'+ (data[i][2]) +'</td>\
                            <td>'+ (data[i][3]) +'</td>\
                            <td>'+ (data[i][4]) +'</td>\
                            <td>'+ (data[i][5]) +'</td>\
                            <td>'+ (data[i][6]) +'</td>\
                            <td>'+ (data[i][1]) +'</td>\
                            </tr>';
                        }
                    }
                }

            },
            error: function (e) {
                console.debug(e);
            },
            complete: function (e) {
            }
        });
        }
    }
});
