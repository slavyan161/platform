<?php

use Phalcon\Config;
use Phalcon\Logger;
use App\Models\Menu;

$menu = Menu::find()->toArray();
$array = array();
foreach ($menu as $key => $value) {
    if ($value['link'] != '#') {
        $action=explode(',',$value['action']);
        $array[$value['link']] = $action;
    }
}
$privateResources = array('privateResources' => $array);

return new Config($privateResources);
