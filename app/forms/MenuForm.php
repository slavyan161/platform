<?php
namespace App\Forms;

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Select;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use App\Models\Menu;

class MenuForm extends Form
{

    public function initialize($entity = null, $options = null)
    {
        if (isset($options['edit']) && $options['edit'] || isset($options['action']) && $options['action']) {
            $id = new Hidden('id');
        } else {
            $id = new Text('id', ['class' => 'form-control']);
        }

        $this->add($id);

        if (isset($options['edit']) && $options['edit'] || isset($options['action']) && $options['action']) {
            $menus = Menu::find([
                // '(parent is null OR parent = 0) AND'
                'id != \''.$this->_entity->id.'\'',
            ]);
        } else {
            $menus = Menu::find([
                // '(parent is null OR parent = 0)',
            ]);
        }

        $this->add(new Select('parent', $menus, [
            'using' => [
                'id',
                'name'
            ],
            'useEmpty' => true,
            'emptyText' => '...',
            'emptyValue' => '',
            'class' => 'form-control'
        ]));

        if (isset($options['action']) && $options['action']) {
            $name = new Text('name', [
                'placeholder' => 'Name',
                'class' => 'form-control',
                'readonly' => 'readonly'
            ]);
        } else {
            $name = new Text('name', [
                'placeholder' => 'Name',
                'class' => 'form-control'
            ]);
        }

        $name->addValidators([
            new PresenceOf([
                'message' => 'The name is required'
            ])
        ]);

        $this->add($name);

        $icon = new Text('icon', [
            'placeholder' => 'ex: fa-awesome',
            'class' => 'form-control'
        ]);

        $this->add($icon);

        $link = new Text('link', [
            'placeholder' => 'ex: index',
            'class' => 'form-control'
        ]);

        $link->addValidators([
            new PresenceOf([
                'message' => 'The link is required'
            ])
        ]);

        $this->add($link);

        $action = new Text('action', [
            'placeholder' => 'ex: index,edit,delete',
            'class' => 'form-control'
        ]);

        $this->add($action);
    }
}
