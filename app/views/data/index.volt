<section class="content-header">
    <h1>
        {{ router.getControllerName()|capitalize }}
    </h1>
</section>

<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="box box-default color-palette-box">
        <div class="box-body">
            {{ content() }}

            {% for partner in dataPartner %}
            {% if loop.first %}
            <table class="table table-bordered table-striped" align="center">
                <thead>
                    <tr>
                        <th width="5%">Partner Id</th>
                        <th>Partner Name</th>
                        <th colspan="2">Partner Type</th>
                       
                    </tr>
                </thead>
                <tbody>
                    {% endif %}
                    <tr>
                        <td>{{ partner.partnerId }}</td>
                        <td>{{ partner.partnerName }}</td>
                        <td>{{ partner.partnerTypeName }}</td>
                    </tr>
                    {% if loop.last %}
                </tbody>
            </table>
            {% endif %}
            {% else %}
            You dont have Permissions to see any Data
            {% endfor %}
        </div>
    </div>
</section>

