<section class="content-header">
    <h1>
        {{ router.getControllerName()|capitalize }}
    </h1>
</section>

<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="box box-default color-palette-box box-body table-responsive no-padding">
        <div class="box-body">
            {{ content() }}

            <ul class="pager">
                <li class="previous pull-left">
                    {{ link_to("profiles/index", "&larr; Go Back") }}
                </li>
                <li class="pull-right">
                    {{ link_to("profiles/create", "Create profiles", "class": "btn") }}
                </li>
            </ul>

            {% for profile in page.items %}
            {% if loop.first %}
            <table class="table table-bordered table-striped" align="center">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Active?</th>
                    </tr>
                </thead>
                <tbody>
                    {% endif %}
                    <tr>
                        <td>{{ profile.id }}</td>
                        <td>{{ profile.name }}</td>
                        <td>{{ profile.active == 'Y' ? 'Yes' : 'No' }}</td>
                        <td width="12%">{{ link_to("profiles/edit/" ~ profile.id, '<i class="fa fa-pencil"></i> Edit', "class": "btn btn-default") }}</td>
                        <td width="12%">{{ link_to("profiles/delete/" ~ profile.id, '<i class="fa fa-remove"></i> Delete', "class": "btn btn-default", 'onClick': 'return confirm("Are you sure?");') }}</td>
                    </tr>
                    {% if loop.last %}
                </tbody>
                <tr>
                    <td>{{ page.current }} / {{ page.total_pages }}</td>
                    <td colspan="10" align="right">
                        <div class="btn-group">
                            {{ link_to("profiles/search", '<i class="fa fa-fast-backward"></i> First', "class": "btn btn-default") }}
                            {{ link_to("profiles/search?page=" ~ page.before, '<i class="fa fa-step-backward"></i> Previous', "class": "btn btn-default") }}
                            {{ link_to("profiles/search?page=" ~ page.next, '<i class="fa fa-step-forward"></i> Next', "class": "btn btn-default") }}
                            {{ link_to("profiles/search?page=" ~ page.last, '<i class="fa fa-fast-forward"></i> Last', "class": "btn btn-default") }}
                            <!-- <span class="help-inline">{{ page.current }}/{{ page.total_pages }}</span> -->
                        </div>
                    </td>
                </tr>
            </table>
            {% endif %}
            {% else %}
            No profiles are recorded
            {% endfor %}
        </div>
    </div>
</section>