<section class="content-header">
    <h1>
        {{ router.getControllerName()|capitalize }}
    </h1>
</section>
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="box box-default color-palette-box">
        <div class="box-body">
            {{ content() }}

            <div align="right">
                {{ link_to("profiles/create", "<i class='icon-plus-sign'></i> Create Profiles", "class": "btn btn-primary") }}
            </div>
            <div class="col-md-6">
                <div class="row">
                    <form role="form" method="post" action="{{ url("profiles/search") }}" autocomplete="off">
                        <div class="box-body">
                            <div class="form-group">
                                <label>Id</label>
                                {{ form.render("id") }}
                            </div>
                            <div class="form-group">
                                <label>Name</label>
                                {{ form.render("name") }}
                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            {{ submit_button("Search", "class": "btn btn-primary") }}
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>