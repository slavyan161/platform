<section class="content-header">
    <h1>
        {{ router.getControllerName()|capitalize }}
    </h1>
</section>
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="box box-default color-palette-box">
        <div class="box-body">
            {{ content() }}
            <form method="post" autocomplete="off">

                <ul class="pager">
                    <li class="previous pull-left">
                        {{ link_to("menu", "&larr; Go Back") }}
                    </li>
                    <li class="pull-right">
                        {{ submit_button("Save", "class": "btn btn-success") }}
                    </li>
                </ul>


                <div class="center scaffold">

                    <h2>Edit menu</h2>               

                    <div class="col-md-6">
                        <div class="row">
                            <div class="box-body">
                                {{ form.render("id") }}

                                <div class="form-group">
                                    <label>Parent</label>
                                    {{ form.render("parent") }}
                                </div>
                                <div class="form-group">
                                    <label>Name</label>
                                    {{ form.render("name") }}
                                </div>
                                <div class="form-group">
                                    <label>Icon</label>
                                    {{ form.render("icon") }}
                                </div>
                                <div class="form-group">
                                    <label>Link</label>
                                    {{ form.render("link") }}
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </form>
        </div>
    </div>
</section>