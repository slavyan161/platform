<section class="content-header">
    <h1>
        {{ router.getControllerName()|capitalize }}
    </h1>
</section>

<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="box box-default color-palette-box">
        <div class="box-body">
            {{ content() }}

            <ul class="pager">
                <li class="pull-right">
                    {{ link_to("menu/create", "Create menu", "class": "btn") }}
                </li>
            </ul>

            {% for menu in page.items %}
            {% if loop.first %}
            <table class="table table-bordered table-striped box-body table-responsive no-padding" align="center">
                <thead>
                    <tr align="center">
                        <th><center>Id</center></th>
                        <th><center>Parent Id</center></th>
                        <th><center>Name</center></th>
                        <th><center>Icon</center></th>
                        <th><center>Link</center></th>
                    </tr>
                </thead>
                <tbody>
                    {% endif %}
                    <tr align="center">
                        <td>{{ menu.id }}</td>
                        <td>{{ menu.parent }}</td>
                        <td>{{ menu.name }}</td>
                        <td><i class="{{ menu.icon != null ? menu.icon : 'fa fa-circle-o' }}"></i></td>
                        <td>{{ menu.link }}</td>
                        {% if menu.link == '#' %}
                        <td width="12%">{{ link_to("menu/action/" ~ menu.id, '<i class="fa fa-plus"></i> Action Menu', "class": "btn btn-default disabled") }}</td>
                        {% else %}
                        <td width="12%">{{ link_to("menu/action/" ~ menu.id, '<i class="fa fa-plus"></i> Action Menu', "class": "btn btn-default") }}</td>
                        {% endif %}
                        <td width="12%">{{ link_to("menu/edit/" ~ menu.id, '<i class="fa fa-pencil"></i> Edit', "class": "btn btn-default") }}</td>
                        <td width="12%">{{ link_to("menu/delete/" ~ menu.id, '<i class="fa fa-remove"></i> Delete', "class": "btn btn-default", 'onClick': 'return confirm("Are you sure?");') }}</td>
                    </tr>
                    {% if loop.last %}
                </tbody>
                <tr>
                    <td>{{ page.current }} / {{ page.total_pages }}</td>
                    <td colspan="10" align="right">
                        <div class="btn-group">
                            {{ link_to("menu/index", '<i class="fa fa-fast-backward"></i> First', "class": "btn btn-default") }}
                            {{ link_to("menu/index?page=" ~ page.before, '<i class="fa fa-step-backward"></i> Previous', "class": "btn btn-default") }}
                            {{ link_to("menu/index?page=" ~ page.next, '<i class="fa fa-step-forward"></i> Next', "class": "btn btn-default") }}
                            {{ link_to("menu/index?page=" ~ page.last, '<i class="fa fa-fast-forward"></i> Last', "class": "btn btn-default") }}
                            <!-- <span class="help-inline">{{ page.current }}/{{ page.total_pages }}</span> -->
                        </div>
                    </td>
                </tr>
            </table>
            {% endif %}
            {% else %}
            No profiles are recorded
            {% endfor %}
        </div>
    </div>
</section>