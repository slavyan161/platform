<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Platform | {{ router.getControllerName()|capitalize }}</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.7 -->
	{{ stylesheet_link('assets/bower_components/bootstrap/dist/css/bootstrap.min.css') }}
	<!-- Font Awesome -->
	{{ stylesheet_link('assets/bower_components/font-awesome/css/font-awesome.min.css') }}
	<!-- Ionicons -->
	{{ stylesheet_link('assets/bower_components/Ionicons/css/ionicons.min.css') }}
	<!-- Theme style -->
	{{ stylesheet_link('assets/dist/css/AdminLTE.min.css') }}
	<!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
	{{ stylesheet_link('assets/dist/css/skins/_all-skins.min.css') }}
  <!-- iCheck -->
  {{ stylesheet_link('assets/plugins/iCheck/square/blue.css') }}

  <!-- Add CSS-->
  {{ this.assets.outputCss('headerCss') }}
  {{ this.assets.outputInlineCss('headerCss') }}
  <!-- Add Js -->
  {{ this.assets.outputJs('headerJs') }}
  {{ this.assets.outputInlineJs('headerJs') }}

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  	<!--[if lt IE 9]
  	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  	[endif]-->

  	<!-- Google Font -->
  	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini login-page fixed">
  	{{ content() }}
  	<!-- ./wrapper -->

  	<!-- jQuery 3 -->
    {{ javascript_include('assets/bower_components/jquery/dist/jquery.min.js') }}
  	<!-- jQuery UI 1.11.4 -->
    {{ javascript_include('assets/bower_components/jquery-ui/jquery-ui.min.js') }}
  	<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
  	<script>
  	$.widget.bridge('uibutton', $.ui.button);
  	</script>
  	<!-- Bootstrap 3.3.7 -->
    {{ javascript_include('assets/bower_components/bootstrap/dist/js/bootstrap.min.js') }}
  	<!-- AdminLTE App -->
    {{ javascript_include('assets/dist/js/adminlte.min.js') }}
    {{ javascript_include('assets/dist/js/demo.js') }}
    
    <!-- Add Js -->
    {{ this.assets.outputJs('footerJs') }}
    {{ this.assets.outputInlineJs('footerJs') }}
</body>
</html>