<div class="login-box">
  <div class="login-logo">
    <a href="{{ url('#') }}"><b>Platform</b></a>
  </div>
  <!-- /.login-logo -->
  {{ content() }}
  <!-- /.login-box-body -->
</div>
