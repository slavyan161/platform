<section class="content-header">
	<h1>
		{{ router.getControllerName()|capitalize }}
	</h1>
</section>

<section class="content">
	<!-- Small boxes (Stat box) -->
	<div class="box box-default color-palette-box">
		<div class="box-body">
			{{ content() }}
			<form method="post">
				<div class="col-md-12">
					<div class="row">
						<div class="form-group">
							<label class="col-md-1 control-label">Profile</label>
							<div class="col-md-3">
								{{ select('profileId', profiles, 'class': 'form-control', 'using': ['id', 'name'], 'useEmpty': true, 'emptyText': '...', 'emptyValue': '') }}
							</div>
							<div class="col-md-3">
								{{ submit_button('Search', 'class': 'btn btn-primary', 'name' : 'search') }}
							</div>
						</div>
					</div>

					{% if request.isPost() and profile %}

					{% for resource, actions in acl.getResources() %}

					<h3>{{ resource }}</h3>

					<table class="table table-bordered table-striped" align="center">
						<thead>
							<tr>
								<th width="5%"></th>
								<th>Description</th>
							</tr>
						</thead>
						<tbody>
							{% for action in actions %}
							<tr>
								<td align="center"><input type="checkbox" name="permissions[]" value="{{ resource ~ '.' ~ action }}"  {% if permissions[resource ~ '.' ~ action] is defined %} checked="checked" {% endif %}></td>
								<td>{{ acl.getActionDescription(action) ~ ' ' ~ resource }}</td>
							</tr>
							{% endfor %}
						</tbody>
					</table>

					{% endfor %}

					{{ submit_button('Submit', 'class': 'btn btn-primary', 'name':'submit') }}   

					{% endif %}
				</div>
			</form>
		</div>
	</div>
</section>
