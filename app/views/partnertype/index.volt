<section class="content-header">
    <h1>
        {{ router.getControllerName()|capitalize }}
    </h1>
</section>

<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="box box-default color-palette-box">
        <div class="box-body">
            {{ content() }}

            <ul class="pager">
                <li class="previous pull-left">
                    {{ link_to("partnertype/index", "&larr; Go Back") }}
                </li>
                <li class="pull-right">
                    {{ link_to("partnertype/create", "Create Partner", "class": "btn") }}
                </li>
            </ul>

            {% for partner in page.items %}
            {% if loop.first %}
            <table class="table table-bordered table-striped" align="center">
                <thead>
                    <tr>
                        <th width="5%">Id</th>
                        <th>Name</th>
                        <th colspan="2">Action</th>
                    </tr>
                </thead>
                <tbody>
                    {% endif %}
                    <tr>
                        <td>{{ partner.id }}</td>
                        <td>{{ partner.name }}</td>
                        <td width="12%">{{ link_to("partnertype/edit/" ~ partner.id, '<i class="fa fa-pencil"></i> Edit', "class": "btn btn-default") }}</td>
                        <td width="12%">{{ link_to("partnertype/delete/" ~ partner.id, '<i class="fa fa-remove"></i> Delete', "class": "btn btn-default", 'onClick': 'return confirm("Are you sure?");') }}</td>
                    </tr>
                    {% if loop.last %}
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="10" align="right">
                            <div class="btn-group">
                                {{ link_to("partnertype/index", '<i class="fa fa-fast-backward"></i> First', "class": "btn btn-default") }}
                                {{ link_to("partnertype/index?page=" ~ page.before, '<i class="fa fa-step-backward"></i> Previous', "class": "btn btn-default") }}
                                {{ link_to("partnertype/index?page=" ~ page.next, '<i class="fa fa-step-forward"></i> Next', "class": "btn btn-default") }}
                                {{ link_to("partnertype/index?page=" ~ page.last, '<i class="fa fa-fast-forward"></i> Last', "class": "btn btn-default") }}
                                 <!-- <span class="help-inline">{{ page.current }}/{{ page.total_pages }}</span>  -->
                            </div>
                        </td>
                    </tr>
                </tfoot>
            </table>
            {% endif %}
            {% else %}
            No profiles are recorded
            {% endfor %}
        </div>
    </div>
</section>

