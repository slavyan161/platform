<section class="content-header">
    <h1>
        {{ router.getControllerName()|capitalize }}
    </h1>
</section>
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="box box-default color-palette-box">
        <div class="box-body">
            {{ content() }}
            <form method="post" autocomplete="off">

                <ul class="pager">
                    <li class="previous pull-left">
                        {{ link_to("partner", "&larr; Go Back") }}
                    </li>
                    <li class="pull-right">
                        {{ submit_button("Save", "class": "btn btn-success") }}
                    </li>
                </ul>


                <div class="center scaffold">

                    <h2>Edit Partner</h2>

                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#A" data-toggle="tab">Basic</a></li>
                        <!-- <li><a href="#B" data-toggle="tab">Users</a></li> -->
                    </ul>

                    <div class="tabbable">
                        <div class="tab-content">
                            <div class="tab-pane active" id="A">

                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="box-body">
                                            {{ form.render("id") }}

                                            <div class="form-group">
                                                <label for="name">Name</label>
                                                {{ form.render("name") }}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>
</section>
