<section class="content-header">
    <h1>
        {{ router.getControllerName()|capitalize }}
    </h1>
</section>

<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="box box-default color-palette-box">
        <div class="box-body">
            {{ content() }}
            <form method="post">
            <div class="col-md-3">
            {{ select('partnersId', partnerType, 'class': 'form-control', 'using': ['id', 'name'], 'useEmpty': false, 'emptyText': '...', 'emptyValue': '') }}
            </div>
            <div class="col-md-3">
                {{ submit_button('Search', 'class': 'btn btn-primary', 'name' : 'search') }}
            </div>
            <div class="col-md-3 pull-right">
               {{ link_to("partner/create", "Create Partner", "class": 'btn btn-primary') }}
            </div>
            <br>
            {% if request.isPost() %}
            <br>
            {% set i=0 %}
            {% for partner in partners %}
            {% if loop.first %}
            <table class="table table-bordered table-striped" align="center">
                <thead>
                    <tr>
                        <th width="5%">No</th>
                        <!-- <th width="5%">Id</th> -->
                        <th>Partner Name</th>
                        <th>Partner Type</th>
                        <th colspan="2">Action</th>
                    </tr>
                </thead>
                <tbody>
                    {% endif %}
                    <tr>
                        {% set i = i+1 %}
                        <td>{{ i }}</td>
                        <!-- <td>{{ partner.partnerId }}</td> -->
                        <td>{{ partner.partnerName }}</td>
                        <td>{{ partner.partnerTypeName }}</td>
                        <td width="12%">{{ link_to("partner/edit/" ~ partner.partnerId, '<i class="fa fa-pencil"></i> Edit', "class": "btn btn-default") }}</td>
                        <td width="12%">{{ link_to("partner/delete/" ~ partner.partnerId, '<i class="fa fa-remove"></i> Delete', "class": "btn btn-default", 'onClick': 'return confirm("Are you sure?");') }}</td>
                    </tr>
                    {% if loop.last %}
                </tbody>
            </table>
            {% endif %}
            {% else %}
            No profiles are recorded
            {% endfor %}
        </div>
        {% endif %}
        </form>
    </div>
</section>

