<section class="content-header">
    <h1>
        {{ router.getControllerName()|capitalize }}
    </h1>
</section>
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="box box-default color-palette-box">
        <div class="box-body">
            {{ content() }}
            <form method="post" autocomplete="off">
                <ul class="pager">
                    <li class="previous pull-left">
                        {{ link_to("partner", "&larr; Go Back") }}
                    </li>
                    <li class="pull-right">
                        {{ submit_button("Save", "class": "btn btn-success") }}
                    </li>
                </ul>
                    <h2>Create a Partner</h2>
                    <div class="row">
                        <div class="col-md-3">
                            {{ select('partnersId', partners, 'class': 'form-control', 'using': ['id', 'name'], 'useEmpty': false, 'emptyText': '...', 'emptyValue': '') }}
                            </div>
                        <div class="col-md-5">
                            {{ form.render("name") }}
                        </div>
                    </div>
             

            </form>
        </div>
    </div>
</section>