<div class="login-box-body">
  <p class="login-box-msg">Sign in to start your session</p>
  {{ content() }}

  {{ form() }}
  <div class="form-group has-feedback">
    {{ form.render('email') }}
    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
  </div>
  <div class="form-group has-feedback">
    {{ form.render('password') }}
    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
  </div>
  <div class="row">
    <div class="col-xs-8">
      <div class="checkbox icheck">
        {{ form.render('remember') }}
        {{ form.label('remember') }}
      </div>
      {{ form.render('csrf', ['value': security.getToken()]) }}
    </div>
    <!-- /.col -->
    <div class="col-xs-4">
      {{ form.render('Sign In') }}
    </div>
    <!-- /.col -->
  </div>
</form>

<!-- <a href="#">I forgot my password</a><br>
  <a href="register.html" class="text-center">Register a new membership</a> -->

</div>