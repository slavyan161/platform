<section class="content-header">
	<h1>
		{{ router.getControllerName()|capitalize }}
	</h1>
</section>

<section class="content">
	<!-- Small boxes (Stat box) -->
	<div class="box box-default color-palette-box">
		<div class="box-body">
			{{ content() }}
			<form method="post">
				<div class="col-md-12">
					<div class="row">
						<div class="form-group">
							<label class="col-md-1 control-label">Profile</label>
							<div class="col-md-3">
								{{ select('profileId', profiles, 'class': 'form-control', 'using': ['id', 'name'], 'useEmpty': true, 'emptyText': '...', 'emptyValue': '') }}
							</div>
							{% if request.isPost() and profile %}
								<div class="col-md-3">
								{{ select('partnersId', partners, 'class': 'form-control', 'using': ['id', 'name'], 'useEmpty': true, 'emptyText': '...', 'emptyValue': '') }}
								</div>
							{% endif %}
							<div class="col-md-3">
								{{ submit_button('Search', 'class': 'btn btn-primary', 'name' : 'search') }}
							</div>
						</div>
					</div>

					{% if request.isPost() and profile %}
					{% for users in user %}
					<label for='name'><h3>{{ users.name|capitalize }}</h3></label> 
					<table class="table table-bordered table-striped" align="center">
						<thead>
							<tr>
								<th width="1%">^</th>
								<th width="15%">Partner Name</th>
							</tr>
						</thead>
						<tbody>
						{% set i = 0 %}
							{% for results in result %}
							
							<tr>
								
								<td> <input type="checkbox" name="permissions[]" value="{{users.id ~ '.' ~ results.id ~ '.' ~ results.typeId}}"  
									{% if permissions['partnerId' ~ '.' ~users.id ~ '.' ~ results.id] is defined %} 
										{% if results.id !== 0 OR results.id !== null %}
											 checked="checked"   
										{% endif %} 
									{% endif %} >
									<input type="hidden" name="typePartnersId" value="{{results.typeId}}">
								</td>	
								
								<td> {{results.name}} </td> 

							</tr>
							{% set i = i +1 %}
							{% endfor %}
						</tbody>
					</table>
					{% endfor %}
					{{ submit_button('Submit', 'class': 'btn btn-primary', 'name':'submit') }}   

					{% endif %}
				</div>
			</form>
		</div>
	</div>
</section>
