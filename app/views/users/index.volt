<section class="content-header">
    <h1>
        {{ router.getControllerName()|capitalize }}
    </h1>
</section>

<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="box box-default color-palette-box">
        <div class="box-body">
            {{ content() }}

            <div align="right">
                {{ link_to("users/create", "<i class='icon-plus-sign'></i> Create Users", "class": "btn btn-primary") }}
            </div>

            <div class="col-md-6">
                <div class="row">

                    <form method="post" action="{{ url("users/search") }}" autocomplete="off">

                        <div class="box-body">
                            <div class="form-group">
                                <label for="id">Id</label>
                                {{ form.render("id") }}
                            </div>

                            <div class="form-group">
                                <label for="name">Name</label>
                                {{ form.render("name") }}
                            </div>

                            <div class="form-group">
                                <label for="email">E-Mail</label>
                                {{ form.render("email") }}
                            </div>

                            <div class="form-group">
                                <label for="profilesId">Profile</label>
                                {{ form.render("profilesId") }}
                            </div>

                        </div>
                        
                        <div class="box-footer">
                            {{ submit_button("Search", "class": "btn btn-primary") }}
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</section>