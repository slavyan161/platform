<section class="content-header">
    <h1>
        {{ router.getControllerName()|capitalize }}
    </h1>
</section>
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="box box-default color-palette-box">
        <div class="box-body">
            {{ content() }}

            <form method="post" autocomplete="off" action="{{ url("users/changePassword") }}">

                <div class="col-md-6">
                    <div class="box-body">

                        <h2>Change Password</h2>

                        <div class="form-group">
                            <label for="password">Password</label>
                            {{ form.render("password") }}
                        </div>

                        <div class="form-group">
                            <label for="confirmPassword">Confirm Password</label>
                            {{ form.render("confirmPassword") }}
                        </div>

                        <div class="clearfix">
                            {{ submit_button("Change Password", "class": "btn btn-primary") }}
                        </div>

                    </div>
                </div>

            </form>
        </div>
    </div>
</section>