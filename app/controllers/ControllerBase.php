<?php
namespace App\Controllers;

use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;
use App\Models\Profiles;
class ControllerBase extends Controller
{
	protected $data;
	public function assets()
	{
		//css
		$collection['headerCss'] = $this->assets->collection("headerCss");
		//js
		$collection['headerJs'] = $this->assets->collection("headerJs");
		$collection['footerJs'] = $this->assets->collection("footerJs");

		// $data = 5;
		return $collection;
	}

	public function beforeExecuteRoute(Dispatcher $dispatcher)
	{
		$collection = $this->assets();
		$controllerName =$dispatcher->getControllerName();
		// $controllerName =$dispatcher->getControllerClass();
		// dd($controllerName);
		// $controllerName = explode('"\"', $controllerName);
		// dd($controllerName);
		// dd(Phalcon\Text::camelize("coco_bongo"));
		// $actionName = Phalcon\Text::camelize($dispatcher->getActionName());

        // Only check permissions on private controllers
		if ($this->acl->isPrivate($controllerName)) {

            // Get the current identity
			$identity = $this->auth->getIdentity();

            // If there is no identity available the user is redirected to index/index
			if (!is_array($identity)) {

				$this->flash->notice('You don\'t have access to this module: private');

				$dispatcher->forward([
					'controller' => 'index',
					'action' => 'index'
				]);
				return false;
			}
			//ACL DATA SEND Partner Table
			$dataAcl = $this->acl->getAclData($this->auth->getUserId());
			$this->data = $dataAcl;
			
            // Check if the user have permission to the current option
            
			$actionName = $dispatcher->getActionName();
			// dd($dispatcher);
			if (!$this->acl->isAllowed($identity['profile'], $controllerName, $actionName)) {

				$this->flash->notice('You don\'t have access to this module: ' . $controllerName . ':' . $actionName);
				if ($this->acl->isAllowed($identity['profile'], $controllerName, 'index')) {

					//get data from database for ACL DATA
					$dispatcher->forward([
						'controller' => $controllerName,
						'action' => 'index',
					]);
				} else {
					$dispatcher->forward([
						'controller' => 'user_control',
						'action' => 'index',
					]);
				}

				return false;
			}
		}
	}

	public function refnum()
	{
		$m = explode(' ', microtime());
		list($totalSeconds, $extraMilliseconds) = array($m[1], $m[0] * 100000000);
		$answer_time = date("d-m-Y H:i:s", $totalSeconds) . ".$extraMilliseconds\n";
		$refnum = hash('sha256', $answer_time);
		return $refnum;
	}

	public function inc_mon()
	{
		$inc['SETTLE_DATE_DIFF'] = '0';
		$inc['VSI_DIFF_TIME'] = '0';
		$inc['VSI_TIME_ZONE_LABEL'] = 'WIB';
		$inc['MIN_BALANCE'] = '200000';

		$inc['VSI_TIME_DETAIL'] = '19';
		$inc['VSI_CENTRAL_UPLINE_AREA_NAME'] = 'Danamon Syariah Agent';

		// 1 : based on account number filter, 0 : based on area filter
		$inc['VSI_DEPOSIT_TYPE'] = '0'; 

		$inc['VSI_ACCOUNT_NUMBER'] = array('010110101' => 'BDI');

		return $inc;
	}

	public function getDataACL() {
		return $this->data;
	}
}