<?php
namespace App\Controllers;

use App\Models\Profiles;
use App\Models\Datapermissions;
use App\Models\Users;
use App\Models\Partner;
use App\Models\PartnerType;
use Phalcon\Mvc\Model\Manager as ModelsManager;
use Phalcon\Mvc\Model\Query;


class DataPermissionsController extends ControllerBase
{
    public function initialize()
    {
        $this->view->setTemplateBefore('private');
        $collection = $this->assets();
    }

    public function indexAction()
    {
        $this->view->setTemplateBefore('private');

        if ($this->request->isPost()) {
            $partner = new Partner();
            if($this->request->getPost('partnersId') != NULL || $this->request->getPost('partnersId') != 0 ) {
                $results = $partner->getAllDataByType($this->request->getPost('partnersId'));
            } else {
                // $results = $partner::find();
                $results = $partner->getAllDataByType('3');
            }
            $profile = Profiles::findFirstById($this->request->getPost('profileId'));
            $users = Users::findByProfilesId($this->request->getPost('profileId'));
            foreach ($users as $key) {
                $profilesId = $key->profilesId;
                $usersId = $key->id;
            }
            if ($profile) {

                if ($this->request->hasPost('permissions') && $this->request->hasPost('submit')) {
                    // Deletes the current permissions
                    $datapermissions = Datapermissions::query()
                    ->where("typePartnerId = ".$this->request->getPost('typePartnersId'))
                    ->andWhere("profilesId =".$profilesId)
                    ->execute();
                    $datapermissions->delete();

                    // Save the new permissions
                    foreach ($this->request->getPost('permissions') as $permission) {
                        $parts = explode('.', $permission);

                        $permission = new Datapermissions();
                        $permission->usersId = $parts[0];
                        $permission->profilesId = $profile->id;
                        $permission->partnerId = $parts[1];
                        $permission->typePartnerId = $parts[2];
                        $permission->save();
                    }
                    $this->flash->success('Permissions were updated with success');
                }

                // Pass the current permissions to the view //untuk checked checked
                $this->view->permissions = $this->acl->getDataPermissions($profilesId);
            }
            $this->view->profile = $profile;
            $this->view->result = $results;
            $this->view->user = $users;
        }
        // Pass all the active profiles
        $this->view->profiles = Profiles::find([
            'active = :active:',
            'bind' => [
                'active' => 'Y'
            ]
        ]);
        $this->view->partners = PartnerType::find();
    }

}

