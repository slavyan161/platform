<?php
namespace App\Controllers;

class IndexController extends ControllerBase
{

	public function indexAction()
	{
		$this->view->setVar('logged_in', is_array($this->auth->getIdentity()));
		if(!$this->view->getVar('logged_in')) {
			return $this->dispatcher->forward([
				"controller" => "login",
				"action" => "index"
			]);
		} else {
			return $this->dispatcher->forward([
				"controller" => "data",
				"action" => "index"
			]);
		}
	}

}

