<?php
namespace App\Controllers;

use App\Forms\LoginForm;
use App\Auth\Exception as AuthException;

class LoginController extends ControllerBase
{
	public function initialize()
	{
		$this->view->setTemplateBefore('public');
		$collection = $this->assets();
		$collection['footerJs']->addJs("assets/plugins/iCheck/icheck.min.js");
		$collection['footerJs']->addInlineJs("
			$(function () {
				$('input').iCheck({
					checkboxClass: 'icheckbox_square-blue',
					radioClass: 'iradio_square-blue',
					increaseArea: '100%' /* optional */
				});
			});
			");
	}

	public function indexAction()
	{
		$form = new LoginForm();

		try {
			if (!$this->request->isPost()) {
				if ($this->auth->hasRememberMe()) {
					return $this->auth->loginWithRememberMe();
				}
			} else {

				if ($form->isValid($this->request->getPost()) == false) {
					foreach ($form->getMessages() as $message) {
						$this->flash->error($message);
					}
				} else {

					$this->auth->check([
						'email' => $this->request->getPost('email'),
						'password' => $this->request->getPost('password'),
						'remember' => $this->request->getPost('remember')
					]);

					return $this->response->redirect('data');
				}
			}
		} catch (AuthException $e) {
			$this->flash->error($e->getMessage());
		}

		$this->view->form = $form;
	}

    public function logoutAction()
    {
        $this->auth->remove();

        return $this->response->redirect('index');
    }
}

