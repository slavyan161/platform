<?php
namespace App\Controllers;

use Phalcon\Tag;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;
use App\Forms\MenuForm;
use App\Models\Menu;
use App\Models\Permissions;

/**
 * App\Controllers\ProfilesController
 * CRUD to manage profiles
 */
class MenuController extends ControllerBase
{

    /**
     * Default action. Set the private (authenticated) layout (layouts/private.volt)
     */
    public function initialize()
    {
        $this->view->setTemplateBefore('private');
    }

    /**
     * Default action, shows the search form
     */
    public function indexAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, 'App\Models\Menu', $this->request->getPost());
            $this->persistent->searchParams = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = [];
        if ($this->persistent->searchParams) {
            $parameters = $this->persistent->searchParams;
        }

        $menu = Menu::find($parameters);
        if (count($menu) == 0) {

            $this->flash->notice("The search did not find any menu");

            return $this->dispatcher->forward([
                "action" => "index"
            ]);
        }
        $paginator = new Paginator([
            "data" => $menu,
            "limit" => 10,
            "page" => $numberPage
        ]);
        // dd( $menu);
        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Creates a new Profile
     */
    public function createAction()
    {
        if ($this->request->isPost()) {

            $menu = new Menu([
                'parent' => $this->request->getPost('parent', 'int'),
                'name' => $this->request->getPost('name', 'striptags'),
                'icon' => $this->request->getPost('icon', 'striptags'),
                'link' => $this->request->getPost('link', 'striptags'),
                'action' => 'index'
            ]);

            if (!$menu->save()) {
                $this->flash->error($menu->getMessages());
            } else {
                $this->flash->success("Menu was created successfully");
            }

            $form = new MenuForm;
            $form->clear();
        }

        $this->view->form = new MenuForm(null);
    }

    /**
     * Edits an existing Profile
     *
     * @param int $id
     */
    public function editAction($id)
    {
        //get identity for profilesId
        $identity = $this->auth->getIdentity();

        //get menus
        $menus = Menu::findFirstById($id);

        //get resource permission by resource and profilesId
        $permissions = Permissions::findByResource($menus->link);

        // dd($permissions);
        // dd($menus);

        if (!$menus) {
            $this->flash->error("Menu was not found");
            return $this->dispatcher->forward([
                'action' => 'index'
            ]);
        }

        if ($this->request->isPost()) {

            $menus->assign([
                'parent' => $this->request->getPost('parent', 'int'),
                'name' => $this->request->getPost('name', 'striptags'),
                'icon' => $this->request->getPost('icon', 'striptags'),
                'link' => $this->request->getPost('link', 'striptags')
            ]);
            foreach ($permissions as $key) {
                $key->resource = $this->request->getPost('link', 'striptags');
                $key->save();
            }
            
            $form = new menuForm($menus, [
                'edit' => true
            ]);

            if ($form->isValid($this->request->getPost()) == false) {

                foreach ($form->getMessages() as $message) {
                    $this->flash->error($message);
                }
                
            } else {

                if (!$menus->save()) {
                    $this->flash->error($menus->getMessages());
                } else {
                     $this->flash->success("Menu was updated successfully");
                     $this->view->form = new MenuForm($menus, [
                        'edit' => true
                     ]);
                    $form->clear();
                    return header("refresh:3; url=".$this->url->getBaseUri()."menu");
                }
            }
        }

        $this->view->form = new MenuForm($menus, [
                'edit' => true
            ]);

        $this->view->menus = $menus;

    }

    /**
     * Deletes a Profile
     *
     * @param int $id
     */
    public function deleteAction($id)
    {
        $menu = Menu::findFirstById($id);
        if (!$menu) {

            $this->flash->error("Menu was not found");

            return $this->dispatcher->forward([
                'action' => 'index'
            ]);
        }

        if (!$menu->delete()) {
            $this->flash->error($menu->getMessages());
        } else {
            $this->flash->success("Menu was deleted");
        }

        $this->dispatcher->forward([
            'action' => 'index'
        ]);

        return header("refresh:5; url=".$this->url->getBaseUri()."menu");
    }

    /**
     * Creates a new Profile
     */
    public function actionAction($id)
    {
        $menus = Menu::findFirstById($id);

        if (!$menus) {
            $this->flash->error("Menu was not found");
            return $this->dispatcher->forward([
                'action' => 'index'
            ]);
        }

        if ($this->request->isPost()) {

            $action = $this->request->getPost('action', 'striptags');
            $ar_action = explode(',', $action);
            $link = $this->request->getPost('link', 'striptags');
            $data = Permissions::find([
                'columns' => 'action',
                'conditions' => 'resource = "'.$link.'"'
            ]);
            $found = false;
            $ar_data = array();
            foreach ($data as $key => $value) {
                array_push($ar_data, $value['action']);
            }
            $ar_data = implode(',', $ar_data);
            if (count($ar_action) < count($data)) {
                $found = true;
            } else {
                foreach ($data as $key => $value) {
                    if (!in_array($value['action'], $ar_action)) {
                        $found = true;
                    }
                }
            }

            if (!$found) {
                $menus->assign([
                    'link' => $this->request->getPost('link', 'striptags'),
                    'action' => $this->request->getPost('action', 'striptags')
                ]);

                $form = new menuForm($menus, [
                    'action' => true
                ]);

                if ($form->isValid($this->request->getPost()) == false) {

                    foreach ($form->getMessages() as $message) {
                        $this->flash->error($message);
                    }

                } else {

                    if (!$menus->save()) {
                        $this->flash->error($menus->getMessages());
                    } else {
                        $this->flash->success("Menu was updated successfully");

                        $form->clear();
                    }

                }
            } else {
                $this->flash->error("Action cannot be updated because realation");
            }

        }

        $this->view->form = new MenuForm($menus, [
            'action' => true
        ]);

        $this->view->menus = $menus;
    }
}
