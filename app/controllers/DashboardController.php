<?php
namespace App\Controllers;

class DashboardController extends ControllerBase
{

	public function initialize()
	{
		$this->view->setTemplateBefore('private');
		$collection = $this->assets();
		//Css
		$collection['headerCss']->addCss('assets/bower_components/morris.js/morris.css');
		$collection['headerCss']->addCss('assets/bower_components/jvectormap/jquery-jvectormap.css');
		$collection['headerCss']->addCss('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css');
		$collection['headerCss']->addCss('assets/bower_components/bootstrap-daterangepicker/daterangepicker.css');
		$collection['headerCss']->addCss('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css');
		//footerJs
		$collection['footerJs']->addJs('assets/bower_components/raphael/raphael.min.js');
		$collection['footerJs']->addJs('assets/bower_components/morris.js/morris.min.js');
		$collection['footerJs']->addJs('assets/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js');
		$collection['footerJs']->addJs('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js');
		$collection['footerJs']->addJs('assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js');
		$collection['footerJs']->addJs('assets/bower_components/jquery-knob/dist/jquery.knob.min.js');
		$collection['footerJs']->addJs('assets/bower_components/moment/min/moment.min.js');
		$collection['footerJs']->addJs('assets/bower_components/bootstrap-daterangepicker/daterangepicker.js');
		$collection['footerJs']->addJs('assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js');
		$collection['footerJs']->addJs('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js');
		$collection['footerJs']->addJs('assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js');
		$collection['footerJs']->addJs('assets/bower_components/fastclick/lib/fastclick.js');
		$collection['footerJs']->addJs('assets/dist/js/pages/dashboard.js');
	}

	public function indexAction()
	{
	}
}

