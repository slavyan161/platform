<?php
namespace App\Controllers;

use App\Models\Profiles;
use App\Models\Permissions;
use Phalcon\Mvc\Dispatcher;

/**
 * View and define permissions for the various profile levels.
 */
class DataController extends ControllerBase
{
	public function initialize()
	{
		$this->view->setTemplateBefore('private');
		$collection = $this->assets();

	}

    /**
     * View the permissions for a profile level, and change them if we have a POST.
     */
    public function indexAction()
    {
        $this->view->setTemplateBefore('private');

        //get data acl data 
        $this->view->dataPartner = $this->getDataACL();    
    }
}
