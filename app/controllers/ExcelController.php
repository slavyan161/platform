<?php
namespace App\Controllers;

use PHPExcel;
use PHPExcel_IOFactory;

class ExcelController extends ControllerBase
{

	public function initialize()
	{
		$this->view->setTemplateBefore('private');
		$collection = $this->assets();
	}

	public function indexAction()
	{
		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();

		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Something');

		// Rename sheet
		$objPHPExcel->getActiveSheet()->setTitle('Name of Sheet 1');

		// Create a new worksheet, after the default sheet
		$objPHPExcel->createSheet();

		// Add some data to the second sheet, resembling some different data types
		$objPHPExcel->setActiveSheetIndex(1);
		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'More data');

		// Rename 2nd sheet
		$objPHPExcel->getActiveSheet()->setTitle('Second sheet');

		header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
        header("Content-Disposition: attachment; filename=Excel.xls");  //File name extension was wrong
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private",false);
     
     	// Save with path   
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		// $objWriter->save(str_replace(__FILE__,BASE_PATH.'/filename.xlsx',__FILE__));
        $objWriter->save('php://output');
        exit;
	}	
}

