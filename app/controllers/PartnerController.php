<?php
namespace App\Controllers;

use App\Models\Profiles;
use App\Models\Partner;
use App\Models\PartnerType;
use App\Models\Permissions;
use Phalcon\Mvc\Dispatcher;
use App\Forms\PartnerTypeForm;
use Phalcon\Paginator\Adapter\Model as Paginator;

/**
 * View and define permissions for the various profile levels.
 */
class PartnerController extends ControllerBase
{
    public function initialize()
    {
        $this->view->setTemplateBefore('private');
        $collection = $this->assets();
    }

    /**
     * View the permissions for a profile level, and change them if we have a POST.
     */
    public function indexAction()
    {
        $this->view->setTemplateBefore('private');
        $partners = new Partner();
        $partnerType = PartnerType::find();
        if($this->request->isPost()){
            $partnerByProfiles = $partners->getDataByTypeId($this->request->getPost('partnersId'));
            $this->view->partners = $partnerByProfiles;
        }
        $this->view->partnerType = $partnerType;
    }

    public function createAction()
    {   
        $partners = PartnerType::find();
        $this->view->partners = $partners;
        if ($this->request->isPost()) {

            $partnerType = new Partner([
                'name' => $this->request->getPost('name', 'striptags'),
                'typeId' => $this->request->getPost('partnersId')
            ]);
            if (!$partnerType->save()) {
                $this->flash->error($partnerType->getMessages());
            } else {
                $this->flash->success("Profile was created successfully");
            }

            $form = new PartnerTypeForm();
            $form->clear();
        }

        $this->view->form = new PartnerTypeForm(null);
    }

    /**
     * Edits an existing Profile
     *
     * @param int $id
     */
    public function editAction($id)
    {
        $partnerType = Partner::findFirstById($id);
        if (!$partnerType) {
            $this->flash->error("Profile was not found");
            return $this->dispatcher->forward([
                'action' => 'index'
            ]);
        }

        if ($this->request->isPost()) {

            $partnerType->assign([
                'name' => $this->request->getPost('name', 'striptags'),
            ]);

            if (!$partnerType->save()) {
                $this->flash->error($partnerType->getMessages());
            } else {

                $this->flash->success("Profile was updated successfully");
            }

            $form = new PartnerTypeForm;
            $form->clear();
        }

        $this->view->form = new PartnerTypeForm($partnerType, [
            'edit' => true
        ]);

        $this->view->partnerType = $partnerType;
    }

    /**
     * Deletes a Profile
     *
     * @param int $id
     */
    public function deleteAction($id)
    {
        $partnerType = Partner::findFirstById($id);
        if (!$partnerType) {

            $this->flash->error("Partner was not found");

            return $this->dispatcher->forward([
                'action' => 'index'
            ]);
        }
        if (!$partnerType->delete()) {
            $this->flash->error($partnerType->getMessages());
        } else {
            $this->flash->success("Partner was deleted");
        }

        return $this->dispatcher->forward([
            'action' => 'index'
        ]);
    }
	
}
