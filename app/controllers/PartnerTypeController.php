<?php
namespace App\Controllers;

use App\Models\Profiles;
use App\Models\Partner;
use App\Models\PartnerType;
use App\Models\Permissions;
use Phalcon\Mvc\Dispatcher;
use App\Forms\PartnerTypeForm;
use Phalcon\Paginator\Adapter\Model as Paginator;

/**
 * View and define permissions for the various profile levels.
 */
class PartnerTypeController extends ControllerBase
{
	public function initialize()
    {
        $this->view->setTemplateBefore('private');
        $collection = $this->assets();
    }

    /**
     * View the permissions for a profile level, and change them if we have a POST.
     */
    public function indexAction()
    {
        $this->view->setTemplateBefore('private');
        
        // $partnersType = PartnerType::find();

        // $paginator = new Paginator([
        //     "data" => $partnersType,
        //     "limit" => 10,
        //     "page" => $numberPage
        // ]);

        // $this->view->page = $paginator->getPaginate();
        // $this->view->partners = $partnersType;

        // TEST PAGINATION
         $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, 'App\Models\PartnerType', $this->request->getPost());
            $this->persistent->searchParams = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = [];
        if ($this->persistent->searchParams) {
            $parameters = $this->persistent->searchParams;
        }

        $partnersType = PartnerType::find($parameters);
        if (count($partnersType) == 0) {

            $this->flash->notice("The search did not find any partner");

            return $this->dispatcher->forward([
                "action" => "index"
            ]);
        }

        $paginator = new Paginator([
            "data" => $partnersType,
            "limit" => 10,
            "page" => $numberPage
        ]);

        $this->view->page = $paginator->getPaginate();
    }

    public function createAction()
    {
        if ($this->request->isPost()) {

            $partnerType = new PartnerType([
                'name' => $this->request->getPost('name', 'striptags'),
            ]);

            if (!$partnerType->save()) {
                $this->flash->error($partnerType->getMessages());
            } else {
                $this->flash->success("Profile was created successfully");
            }

            $form = new PartnerTypeForm();
            $form->clear();
        }

        $this->view->form = new PartnerTypeForm(null);
    }

    /**
     * Edits an existing Profile
     *
     * @param int $id
     */
    public function editAction($id)
    {
        $partnerType = PartnerType::findFirstById($id);
        if (!$partnerType) {
            $this->flash->error("Profile was not found");
            return $this->dispatcher->forward([
                'action' => 'index'
            ]);
        }

        if ($this->request->isPost()) {

            $partnerType->assign([
                'name' => $this->request->getPost('name', 'striptags'),
            ]);

            if (!$partnerType->save()) {
                $this->flash->error($partnerType->getMessages());
            } else {

                $this->flash->success("Profile was updated successfully");
            }

            $form = new PartnerTypeForm;
            $form->clear();
        }

        $this->view->form = new PartnerTypeForm($partnerType, [
            'edit' => true
        ]);

        $this->view->partnerType = $partnerType;
    }

    /**
     * Deletes a Profile
     *
     * @param int $id
     */
    public function deleteAction($id)
    {
        $partnerType = PartnerType::findFirstById($id);
        if (!$partnerType) {

            $this->flash->error("Partner was not found");

            return $this->dispatcher->forward([
                'action' => 'index'
            ]);
        }
        if (!$partnerType->delete()) {
            $this->flash->error($partnerType->getMessages());
        } else {
            $this->flash->success("Partner was deleted");
        }

        return $this->dispatcher->forward([
            'action' => 'index'
        ]);
    }
}
