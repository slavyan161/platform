<?php

use Phalcon\Mvc\View;
use Phalcon\Crypt;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\View\Engine\Php as PhpEngine;
use Phalcon\Mvc\Url as UrlResolver;
use Phalcon\Mvc\View\Engine\Volt as VoltEngine;
use Phalcon\Mvc\Model\Metadata\Memory as MetaDataAdapter;
use Phalcon\Session\Adapter\Files as SessionAdapter;
use Phalcon\Flash\Direct as Flash;
use Phalcon\Logger\Adapter\File as FileLogger;
use Phalcon\Logger\Formatter\Line as FormatterLine;
use App\Auth\Auth;
use App\Acl\Acl;
use App\Models\Menu;


/**
 * Shared configuration service
 */
$di->setShared('config', function () {
    return include APP_PATH . "/config/config.php";
});

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->setShared('url', function () {
    $config = $this->getConfig();

    $url = new UrlResolver();
    $url->setBaseUri($config->application->baseUri);

    return $url;
});

$di->set('getMenu', function ($data, $parent = 0) {
    $controller = $this->get('router')->getControllerName();
    $baseUri = $this->getConfig()->application->baseUri;
    if (isset($data[$parent])) {
        $html = "";
        foreach ($data[$parent] as $v) {
            if ($v['icon'] == null) {
                $icon = 'fa fa-circle-o';
            } else {
                $icon = $v['icon'];
            }
            if ($v['child'] == null) {
                if ($controller == $v['link']) {
                    $html .= "<li class=\"active\">";    
                } else {
                    $html .= "<li>";
                }
                $html .= '<a href="'.$baseUri.$v['link'].'"><i class="'.$icon.'"></i> <span>'.$v['name'].'</a></span>';
                $html .= '</li>';
            } else {
                $ar_child = array();
                $child = $this->get('getMenu', [$data, $v['id']]);
                foreach ($v['child'] as $key => $value) {
                    array_push($ar_child, $value['link']);
                }
                $str = substr($child, 0, 28);
                if (in_array($controller, $ar_child)) {
                    $html .= "<li class=\"active treeview\">";
                } else {
                    $active = '<li class="active treeview">';
                    $find = strpos($child, $active);
                    if ($find !== FALSE) {
                        $html .= "<li class=\"active treeview\">";
                    } else {
                        $html .= "<li class=\"treeview\">";
                    }
                }
                $html .= '<a href="'.$baseUri.$v['link'].'"><i class="'.$icon.'"></i> <span>'.$v['name'].'</span>
                <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
                </span>
                </a>';
                if ($child) {
                    $html .= '<ul class="treeview-menu">';
                    $html .= $child;
                    $html .= '</ul>';
                }
                $html .= '</li>';
            }
        }
        return $html;
    } else {
        return false;
    }
});

/**
 * Setting up the view component
 */
$di->setShared('view', function () {
    $config = $this->getConfig();

    $view = new View();
    $view->setDI($this);
    $view->setViewsDir($config->application->viewsDir);

    $parent = Menu::find([
        'order' => 'name ASC'
    ]);

    $data1 = $parent->toArray();
    foreach ($data1 as $key => $value) {
        $childs = Menu::find([
            'conditions' => 'parent = ' . $value['id'] . ''
        ]);
        $value['child'] = $childs->toArray();
        $data[$value['parent']][] = $value;
    }

    $menu = $this->get('getMenu', [$data]);

    //menu
    $view->setVar('menu', $menu);

    $view->registerEngines([
        '.volt' => function ($view) {
            $config = $this->getConfig();

            $path = $config->application->cacheDir . 'volt/';
            if (!is_dir($path)) {
                mkdir($path);
            }

            $volt = new VoltEngine($view, $this);

            $volt->setOptions([
                'compiledPath' => $path,
                'compiledSeparator' => '_'
            ]);

            $compiler = $volt->getCompiler();
            $compiler->addFunction('dd', 'dd');
            $compiler->addFunction('array_push', 'array_push');

            return $volt;
        },
        '.phtml' => PhpEngine::class

    ]);

    return $view;
});

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->setShared('db', function () {
    $config = $this->getConfig();

    $class = 'Phalcon\Db\Adapter\Pdo\\' . $config->localhost->adapter;
    $params = [
        'host'     => $config->localhost->host,
        'username' => $config->localhost->username,
        'password' => $config->localhost->password,
        'dbname'   => $config->localhost->dbname,
        'charset'  => $config->localhost->charset
    ];

    if ($config->localhost->adapter == 'Postgresql') {
        unset($params['charset']);
    }

    $connection = new $class($params);

    return $connection;
});

$di->setShared('database', function ($name = 'server') {
    $config = $this->getConfig();

    $db = $config->database[$name];

    $class = 'Phalcon\Db\Adapter\Pdo\\' . $db->adapter;
    $params = [
        'host'     => $db->host,
        'username' => $db->username,
        'password' => $db->password,
        'dbname'   => $db->dbname,
        'charset'  => $db->charset
    ];

    if ($db->adapter == 'Postgresql') {
        unset($params['charset']);
    }

    $connection = new $class($params);

    return $connection;
});


/**
 * If the configuration specify the use of metadata adapter use it or use memory otherwise
 */
$di->setShared('modelsMetadata', function () {
    return new MetaDataAdapter();
});

/**
 * Register the session flash service with the Twitter Bootstrap classes
 */
$di->set('flash', function () {
    return new Flash([
        'error'   => 'alert alert-danger',
        'success' => 'alert alert-success',
        'notice'  => 'alert alert-info',
        'warning' => 'alert alert-warning'
    ]);
});

/**
 * Start the session the first time some component request the session service
 */
$di->setShared('session', function () {
    $session = new SessionAdapter();
    $session->start();

    return $session;
});

/**
 * Crypt service
 */
$di->set('crypt', function () {
    $config = $this->getConfig();

    $crypt = new Crypt();
    $crypt->setKey($config->application->cryptSalt);
    return $crypt;
});

/**
 * Dispatcher use a default namespace
 */
$di->set('dispatcher', function () {
    //Create an event manager
    $eventsManager = new Phalcon\Events\Manager();

    //Attach a listener for type "dispatch"
    $eventsManager->attach("dispatch", function($event, $dispatcher) {
        $controllerName = Phalcon\Text::camelize($dispatcher->getControllerName());
        $dispatcher->setControllerName($controllerName);
    });

    $dispatcher = new Dispatcher();
    $dispatcher->setDefaultNamespace('App\Controllers');

    //Set Event Manager For ControllerName Camelize
    // $dispatcher->setEventsManager($eventsManager);
    
    return $dispatcher;
});

/**
 * Custom authentication component
 */
$di->set('auth', function () {
    return new Auth();
});

/**
 * Setup the private resources, if any, for performance optimization of the ACL.  
 */
$di->setShared('AclResources', function() {
    $pr = [];
    if (is_readable(APP_PATH . '/resources/privateResources.php')) {
        $pr = include APP_PATH . '/resources/privateResources.php';
    }
    return $pr;
});

/**
 * Access Control List
 * Reads privateResource as an array from the config object.
 */
$di->set('acl', function () {
    $acl = new Acl();
    $pr = $this->getShared('AclResources')->privateResources->toArray();
    $acl->addPrivateResources($pr);
    return $acl;
});

/**
 * Logger service
 */
$di->set('logger', function ($filename = null, $format = null) {
    $config = $this->getConfig();

    $format   = $format ?: $config->get('logger')->format;
    $filename = trim($filename ?: $config->get('logger')->filename, '\\/');
    $path     = rtrim($config->get('logger')->path, '\\/') . DIRECTORY_SEPARATOR;

    $formatter = new FormatterLine($format, $config->get('logger')->date);
    $logger    = new FileLogger($path . $filename);

    $logger->setFormatter($formatter);
    $logger->setLogLevel($config->get('logger')->logLevel);

    return $logger;
});