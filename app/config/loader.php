<?php

$loader = new \Phalcon\Loader();

/**
 * We're a registering a set of directories taken from the configuration file
 */
$loader->registerNamespaces([
    'App\Models'      => $config->application->modelsDir,
    'App\Controllers' => $config->application->controllersDir,
    'App\Forms'       => $config->application->formsDir,
    'App'             => $config->application->libraryDir,
]);

$loader->registerClasses(
    array(
        "PHPExcel"         			 => APP_PATH . "/library/PHPExcel/Classes/PHPExcel.php",
        "PHPExcel_IOFactory"         => APP_PATH . "/library/PHPExcel/Classes/PHPExcel/IOFactory.php",
        "PHPExcel_Writer_PDF"        => APP_PATH . "/library/PHPExcel/Classes/PHPExcel/Writer/PDF.php",
        "TCPDF"         			 => APP_PATH . "/library/TCPDF/tcpdf.php",
    )
);

$loader->register();

// Use composer autoloader to load vendor classes
require_once BASE_PATH . '/vendor/autoload.php';