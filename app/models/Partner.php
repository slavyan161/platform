<?php
namespace App\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Relation;
use Phalcon\Mvc\Model\Manager as ModelsManager;
use Phalcon\Mvc\Model\Query;

/**
 * App\Models\Mitra
 * All the profile levels in the application. Used in conjenction with ACL lists
 */
class Partner extends Model
{

    /**
     * ID
     * @var integer
     */
    public $id;

    public $name;

    public $typeId;


    /**
     * Define relationships to Users and Permissions
     */
    public function getAllDataByType($type){
        $partner = new Partner();
        // dd($type);
        $query = $partner::findByTypeId($type);
        return $query;
    }
    
    public function getPartnerData($type){
        $partner = new Partner();
        // dd($type);
        $query = $partner::findByTypeId($type);
        return $query;
    }
    
    public function getData($arrayId){
         $dataAcl = Partner::query()
        ->columns(__NAMESPACE__ .'\Partner.id as partnerId , '.__NAMESPACE__ .'\Partner.name as partnerName , '.__NAMESPACE__ .'\PartnerType.name as partnerTypeName')
        ->Join(__NAMESPACE__ .'\PartnerType', __NAMESPACE__ .'\Partner.typeId = '.__NAMESPACE__ .'\PartnerType.id')
        ->inWhere(__NAMESPACE__ .'\Partner.id', $arrayId)
        ->execute();
        return $dataAcl;
    } 
    public function getDataPartners(){
         $dataAcl = Partner::query()
        ->columns(__NAMESPACE__ .'\Partner.id as partnerId , '.__NAMESPACE__ .'\Partner.name as partnerName , '.__NAMESPACE__ .'\PartnerType.name as partnerTypeName')
        ->Join(__NAMESPACE__ .'\PartnerType', __NAMESPACE__ .'\Partner.typeId = '.__NAMESPACE__ .'\PartnerType.id')
        ->execute();
        return $dataAcl;
    }
    public function getDataByTypeId($typesId){
         $dataAcl = Partner::query()
        ->columns(__NAMESPACE__ .'\Partner.id as partnerId , '.__NAMESPACE__ .'\Partner.name as partnerName , '.__NAMESPACE__ .'\PartnerType.name as partnerTypeName')
        ->join(__NAMESPACE__ .'\PartnerType', __NAMESPACE__ .'\Partner.typeId = '.__NAMESPACE__ .'\PartnerType.id')
        ->where(__NAMESPACE__ .'\Partner.typeId ='.$typesId)
        ->execute();
        // dd($dataAcl);
        return $dataAcl;
    }
   
    public function initialize()
    {
        // $this->hasMany('id', __NAMESPACE__ . '\partnercode', 'parnerId', [
        //     'alias' => 'partnercode',
        //     'foreignKey' => [
        //         'action' => Relation::ACTION_CASCADE
        //     ]
        // ]);
    }
    

}