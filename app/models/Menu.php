<?php
namespace App\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Relation;
use App\Models\Menu;

/**
 * App\Models\Menus
 * All the profile levels in the application. Used in conjenction with ACL lists
 */
class Menu extends Model
{

    /**
     * ID
     * @var integer
     */
    public $id;

    /**
     * ID
     * @var integer
     */
    public $parent;

    /**
     * Name
     * @var string
     */
    public $name;

    /**
     * Name
     * @var string
     */
    public $icon;

    /**
     * Name
     * @var string
     */
    public $link;

    /**
     * Name
     * @var string
     */
    public $action;

    /**
     * Define relationships to Users and Permissions
     */
    public function initialize()
    {
        $this->hasMany('id', __NAMESPACE__ . '\Menu', 'parent', [
            'alias' => 'child',
            'foreignKey' => [
                'action' => Relation::ACTION_CASCADE,
                'message' => 'Menu cannot be deleted'
            ]
        ]);

        $this->hasMany('link', __NAMESPACE__ . '\Permissions', 'resource', [
            'alias' => 'permissions',
            'foreignKey' => [
                'action' => Relation::ACTION_CASCADE,
                'message' => 'Menu cannot be deleted'
            ]
        ]);
    }

    public function buildTree($parent)
    {
        $data = array();
        $menu = array();
        $menu['child'] = array();
        foreach($parent as $par)
        {
            $menu = $par->toArray();

            $par->child = $par->buildTree($par->child);
            $menu['child'] = $par->child;
            $data[] = $menu;
        }
        
        return $data;
    }
}
