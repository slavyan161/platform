<?php
namespace App\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Relation;
use Phalcon\Mvc\Model\Manager as ModelsManager;
use Phalcon\Mvc\Model\Query;

/**
 * App\Models\Mitra
 * All the profile levels in the application. Used in conjenction with ACL lists
 */
class PartnerCode extends Model
{

    /**
     * ID
     * @var integer
     */
    public $id;

    public $code;

    public $partner_id;

   
    public function initialize()
    {
        // $this->hasMany('id', __NAMESPACE__ . '\PartnerCode', 'parnerId', [
        //     'alias' => 'partnercode',
        //     'foreignKey' => [
        //         'action' => Relation::ACTION_CASCADE
        //     ]
        // ]);
    }
    

}