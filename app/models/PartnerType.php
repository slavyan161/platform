<?php
namespace App\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Relation;
use Phalcon\Mvc\Model\Manager as ModelsManager;
use Phalcon\Mvc\Model\Query;

/**
 * App\Models\Mitra
 * All the profile levels in the application. Used in conjenction with ACL lists
 */
class PartnerType extends Model
{

    /**
     * ID
     * @var integer
     */
    public $id;

    public $name;


    /**
     * Define relationships to Users and Permissions
     */
   
    public function initialize()
    {
        // $this->hasMany('id', __NAMESPACE__ . '\partnercode', 'parnterId', [
        //     'alias' => 'partnercode',
        //     'foreignKey' => [
        //         'action' => Relation::ACTION_CASCADE
        //     ]
        // ]);
        // $this->hasMany('id', __NAMESPACE__ . '\partnertype', 'type', [
        //     'alias' => 'partnertype',
        //     'foreignKey' => [
        //         'action' => Relation::ACTION_CASCADE
        //     ]
        // ]);
    }
    

}