<?php
namespace App\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Relation;
use App\Models\DataPermissions;

/**
 * App\Models\Menus
 * All the profile levels in the application. Used in conjenction with ACL lists
 */
class DataPermissions extends Model
{

    /**
     * ID
     * @var integer
     */
    public $id;
    public $profilesId;
    public $mitraId;
    public $agentId;
    public $loketId;

    /**
     * Define relationships to Users and Permissions
     */
    public function initialize()
    {
  
    }

}